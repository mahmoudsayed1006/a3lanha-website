import batman from '../images/batman.jpg';

let user = {
    username: "mohamed azzam",
    email: "azzam@gmail.com",
    phone: "01130884193",
    country: "EGYPT",
    type: "CLIENT",
    img: batman
}

let ads = [
    { name: "motors", numbers: 12 },
    { name: "classifieds", numbers: 30 },
    { name: "Property for sales", numbers: 5 },
    { name: "Jobs", numbers: 13 },
    { name: "Job wanted", numbers: 8 },
    { name: "Community", numbers: 4 }
]

export {
    user,
    ads
}