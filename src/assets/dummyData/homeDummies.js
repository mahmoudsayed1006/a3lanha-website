import apartment from '../images/apartmentforSale.jpg'
import plates from '../images/numberPlates.jpg'
import appliance from '../images/homeAppliance.jpg'
import clothing from '../images/clothing.jpg'
import electronics from '../images/Electronics.jpg'
let data = {
    trending: {
        title: "Trending",
        cards: [{
            image:  plates,
            title: {en:"numberPlates",ar:"أرقام لوحات"},
            category: {en:"motor",ar:"موتور"}
        },
        {
            image: apartment,
            title: {en:"appartment",ar:"وحدات سكانية"},
            category: {en:"A3lanaha",ar:"أعلانها"}
        },
        {
            image: appliance,
            title:{en:"appliance",ar:"أدوات منزلية"} ,
            category: {en:"A3lanaha",ar:"أعلانها"}
        },
        {
            image: clothing,
            title: {en:"clothing & accessories",ar:"ملابس وأكساسوارات"} ,
            category: {en:"clothes",ar:"ملابس"}
        },
        {
            image: electronics,
            title: {en:"appliance",ar:"ألكترونيات"} ,
            category: {en:"electronics",ar:"الكترونيات"}
        },
        {
            image: appliance,
            title: {en:"appliance",ar:"ألكترونيات"} ,
            category: {en:"electronics",ar:"الكترونيات"}
        }
        // ,
        // {
        //     image: batman,
        //     title: "i phone x max",
        //     category: "mobile phone"
        // },
        // {
        //     image: batman,
        //     title: "okra",
        //     category: "vegtables"
        // },
        // {
        //     image: batman,
        //     title: "adham",
        //     category: "human"
        // },
        // {
        //     image: batman,
        //     title: "wow",
        //     category: "entertainment"
        // }
    ]
    },
    popular: {
        title: "Popular",
        category: [{
            title: "motors",
            results : 566,
            subCategories: [
                "user for sales",
                "number plates",
                "auto accessories",
                "motorcycles",
                "boats",
                "heavy vehicles"
            ]
        },
        {
            title: "Property for Sale ",
            results : 566,
            subCategories: [
                "user for sales",
                "number plates",
                "auto accessories",
                "motorcycles",
                "boats",
                "heavy vehicles",
                "apartment for sales",
                "land for sale",
            ]
        },
        {
            title: "Property New Projects",
            results : 566,
            isNew: true,
            subCategories: [
                "heavy vehicles",
                "apartment for sales",
                "land for sale",
            ]
        },
        {
            title: "Property New Projects",
            results : 566,
            isNew: true,
            subCategories: [
                "heavy vehicles",
                "apartment for sales",
                "land for sale",
                "apartment for sales",
                "land for sale",
                "apartment for sales",
                "land for sale",
                "apartment for sales",
                "land for sale",
            ]
        }
        ]
    }
}

export {
    data
};