
export const endPoint = "https://a3lanha.herokuapp.com/api/v1/" ;
export const socketEndPoint = "https://a3lanha.herokuapp.com/chat" ;


export const localStorageUserToken = "a3lenha_token" ;
export const localStorageUser = "a3lenha_currentUser" ; 
export const localStorageCurrentLanguage = "a3lenha_currentLanguage" ; 
export const localStorageUserSearch = "a3lenha_userSearch" ;
export const localStorageDefaultCountry = "a3lenha_default_country" ;
export const localStorageCurrentCity ="a3lenha_current_city" ; 
