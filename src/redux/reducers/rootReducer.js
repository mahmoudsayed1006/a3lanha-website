import { combineReducers } from 'redux' ; 

import headerReducer from './headerReducer' ; 
import userReducer from './userReducer' ; 

export default combineReducers({
    headerReducer , 
    userReducer ,
})