import * as types from '../types';
import { localStorageUser } from "../../assets/apiEndPoint";
const initailState = {
    user: localStorage.getItem(localStorageUser) ? JSON.parse(localStorage.getItem(localStorageUser)) : null,
    logged: localStorage.getItem(localStorageUser) ? true : false, 
    following:[] ,
    currentCountry:null,
    currentCity:null,

}

const UserReducer = (state = initailState, action) => {
    switch (action.type) {
        case types.SELECTED_CITY:
            return{
                ...state,currentCity:action.payload
            }
        case types.SELECTED_COUNTRY:
            return{
                ...state,currentCountry:action.payload
            }    
        case types.SAVE_USER:
            return {
                ...state,
                user: action.payload,
                logged: true
            }
        case types.LOGOUT:
            return {
                ...state,
                user: null,
                logged: false
            }   
        default:
            return state;
    }
}

export default UserReducer;