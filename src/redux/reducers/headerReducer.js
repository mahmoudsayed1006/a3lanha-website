import * as types from '../types' ; 
import {localStorageCurrentLanguage} from '../../assets/apiEndPoint' ; 

const initailState = { 
    appLanguage : localStorage.getItem(localStorageCurrentLanguage) ? localStorage.getItem(localStorageCurrentLanguage) : "en" , 
    UAEcities : null , 
    categories : [] , 
    countries : [] ,
    // currentCity : localStorage.getItem(localStorageCurrentCity) ? localStorage.getItem(localStorageCurrentCity) : "all" 
    currentCity : "all",
    categoriesLoading:false,
}

const HeaderReducer = (state = initailState , action ) => {
    switch( action.type ) {
        case types.CHANGE_LANGUAGE :
            localStorage.setItem(localStorageCurrentLanguage , state.appLanguage === "en" ? "ar" : "en" ) ;
            return {
                ...state , 
                appLanguage : state.appLanguage === "en" ? "ar" : "en"
            }
        case types.CHANGE_LANGUAGE_WITH_PARAMS :
            return {
                ...state , 
                appLanguage : action.payload
            }    
        case types.FETCH_CITIES : 
            return {
                ...state , 
                UAEcities : action.payload ,
                currentCity : "all"
            }
        case types.SET_CURRENT_CITY :
            return {
                ...state , 
                currentCity : action.payload === "all" ? "all" : action.payload
            }  
        case types.CATEGORIES_LOADING:
            return{
                ...state,categoriesLoading:true
            }       
        case types.FETCH_CATEGORTIES :
            return {
                ...state , 
                categories : action.payload,
                categoriesLoading:false,
            }
        case types.FETCH_COUNTRIES : 
            return {
                ...state , 
                countries : action.payload  
            }           
        default :
            return state
    }
}

export default HeaderReducer ; 
