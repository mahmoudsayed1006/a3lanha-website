import {
    CHANGE_LANGUAGE,
    CHANGE_LANGUAGE_WITH_PARAMS ,
    FETCH_CITIES ,
    FETCH_CATEGORTIES ,
    FETCH_COUNTRIES ,
    SET_CURRENT_CITY,
    CATEGORIES_LOADING
} from '../types';

import axios from "axios";
import { endPoint , 
         localStorageDefaultCountry ,
         localStorageCurrentCity } from "../../assets/apiEndPoint";

export function changeLanguage() {
    return (dispatch, getState) => {
        dispatch({ type: CHANGE_LANGUAGE });
    }
}

export function changeLanguageWithParams(language) {
    return (dispatch, getState) => {
        dispatch({ type: CHANGE_LANGUAGE_WITH_PARAMS, payload: language });
    }
}

export function fetchCities(countryId) {
    return (dispatch, getState) => {

        if(countryId === null || typeof countryId === "undefined" ) countryId = 1    // UAE ID 

        let uri = `${endPoint}countries/${countryId}/cities`;
        axios.get(uri, {}).then(response => {
            
            localStorage.setItem(localStorageDefaultCountry , countryId ) ;
            localStorage.setItem(localStorageCurrentCity , "all" ) ; 
            dispatch( { type : FETCH_CITIES , payload : response.data.data })
        })
    }
}

export function setFilterCity(index) {
    return(dispatch, getState) => {
        dispatch({ type : SET_CURRENT_CITY , payload:index }) ;
    }
}

export function fetchCategories() {
    return(dispatch , getState) => {
        let uri = `${endPoint}categories?main=true` ; 
        axios.get(uri)
             .then(response => {
                 dispatch({ type : FETCH_CATEGORTIES , payload : response.data.data}) ;
             })
             .catch(err => console.log("err" , err) ) ;
    }
}

export function fetchCountries() {
    return(dispatch,getState) => {
        dispatch({ type : CATEGORIES_LOADING})
        let uri = `${endPoint}countries` ; 
        axios.get(uri)
             .then( response => {
                 dispatch({ type : FETCH_COUNTRIES ,  payload :response.data.data })
             })
             .catch( err => console.log("err" , err) ) ;
    }
}