import {
    SAVE_USER,
    LOGOUT,
    SELECTED_COUNTRY,
    SELECTED_CITY
} from '../types';

import { localStorageUserToken, localStorageUser, endPoint } from '../../assets/apiEndPoint';
import axios from 'axios';

export function saveUser(token, userData) {
    return (dispatch, getState) => {

        localStorage.setItem(localStorageUserToken, token);
        localStorage.setItem(localStorageUser, JSON.stringify(userData));

        dispatch({ type: SAVE_USER, payload: userData });
    }
}

export function logOut() {
    return (dispatch, getState) => {

        localStorage.removeItem(localStorageUserToken);
        localStorage.removeItem(localStorageUser);

        dispatch({ type: LOGOUT })
    }
}

export function addToFavourites(id) {
    return (dispatch, getState) => {

        let uri = `${endPoint}/favourite/${id}/ads`;
        axios.post(uri, null, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(localStorageUserToken)}`
            }
        })
            .then(response => {
                localStorage.setItem(localStorageUser, JSON.stringify(response.data.user));
                dispatch({ type: SAVE_USER, payload: response.data.user });
            })
            .catch(err => console.log("error", err));
    }
}

export function removeFromFav(id) {
    return (dispatch, getState) => {

        let uri = `${endPoint}favourite/${id}`;
        axios.delete(uri, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(localStorageUserToken)}`
            }
        })
            .then(response => {
                localStorage.setItem(localStorageUser, JSON.stringify(response.data.user));
                dispatch({ type: SAVE_USER, payload: response.data.user });
            })
            .catch(err => console.log("error", err));
    }
}

export function followUser(userId) {
    return (dispatch, getState) => {
        let uri = `${endPoint}follow/${userId}/follow`;
        axios.post(uri, null , {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(localStorageUserToken)}`
            }
        })
            .then(response => {
                localStorage.setItem(localStorageUser, JSON.stringify(response.data.myUser));
                dispatch({ type: SAVE_USER, payload: response.data.myUser });
            })
            .catch(err => console.log("error", err));
    }
}

export function unfollowUser(userId) {
    return (dispatch, getState) => {
        let uri = `${endPoint}follow/${userId}/unfollow`;
        axios.put(uri, null , {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(localStorageUserToken)}`
            }
        })
            .then(response => {
                localStorage.setItem(localStorageUser, JSON.stringify(response.data.myuser));
                dispatch({ type: SAVE_USER, payload: response.data.myuser });
            })
            .catch(err => console.log("error", err));
    }
}


export function currentCountryFun(country) {
    return (dispatch, getState) => {
        dispatch({ type: SELECTED_COUNTRY, payload: country });
        localStorage.setItem('CURRENT_COUNTRY',JSON.stringify(country))
        console.log("country   ",country)
}
}

export function currentCityFun(city) {
    return (dispatch, getState) => {
        dispatch({ type: SELECTED_CITY, payload: city });
        localStorage.setItem('CURRENT_CITY',JSON.stringify(city))

}
}