import React, { Component } from 'react';

import './App.css';
import './assets/font.css' ;
import { Route, Switch, BrowserRouter } from 'react-router-dom' ;
import { connect } from 'react-redux';
import Header from './views/header/header';
import Footer from './views/footer/footer';
import Auth from './views/auth/auth' ; 
import Register from './views/auth/register/register';
import Login from './views/auth/login/login';
import Email from './views/forgetPassword/email/email';
import resetpassword from './views/forgetPassword/resetpassword/resetpassword';
import Home from './views/home/home';
import MyProfile from './views/account/myprofile';
import MyAds from './views/account/myAds';
import MyConnections from './views/account/myConnections';
import MyFavourite from './views/account/myFavourites';
import UpdateProfile from './views/account/updateProfile' ; 
import NotFound from './views/404/404'; 
import Confirmationcode from './views/forgetPassword/confirmationcode/confirmationcode'  ;
import AboutUs from './views/Company/AboutUs/AboutUs'
import TermsofUse from './views/Company/TermsofUse/TermsofUse'
import PrivacyPolicy from './views/Company/PrivacyPolicy/PrivacyPolicy'
import Usage from './views/Company/Usage/Usage' ;
import ContactUs from './views/ContactUs/ContactUs' ;
import SearchResult from './views/searchResult/searchResult' ; 
import PlaceAd from './views/ads/placeAd/placeAd' ; 
import ViewMyAd from './views/ads/viewAd/viewAd' ; 
import placeAdinputs from './views/ads/placeAd/placeAdinputs';
import PlaceAdCars from './views/ads/placeAd/placeAdCars' ;
import PlaceAdRealState from './views/ads/placeAd/placeAdRealState' ; 
import PlaceAdJobs from './views/ads/placeAd/placeAdJobs' ; 
import Category from './views/category/category' ; 
import NewDesgin from './views/chat/chat' ;

import { Provider } from 'react-redux'
import {store,persistor } from './redux/store';
import { PersistGate } from 'redux-persist/integration/react'

class App extends Component {

  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
              <Header />
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/auth" component={Auth} />
                <Route path="/auth/register" component={Register} />
                <Route path="/account/myprofile" component={MyProfile} />
                <Route path="/account/myAds" component={MyAds} />
                <Route path="/account/myConnection" component={MyConnections} />
                <Route path="/account/myfavourites" component={MyFavourite} />
                <Route path="/account/updateprofile" component={UpdateProfile} />
                <Route path="/auth/login" component={Login} />
                <Route path="/forgetPassword" component={Email} />
                <Route path="/resetpassword" component={resetpassword} />
                <Route path="/Confirmationcode" component={Confirmationcode} />
                <Route path="/AboutUs" component={AboutUs} />
                <Route path="/TermsofUse" component={TermsofUse} />
                <Route path="/PrivacyPolicy" component={PrivacyPolicy} />
                <Route path="/Usage" component={Usage} />
                <Route path="/ContactUs" component={ContactUs} />
                <Route path="/searchResults/" component={SearchResult} />
                <Route path="/placeYourAds" component={PlaceAd} />
                <Route exact path="/placeAdinputs" component={placeAdinputs} />
                <Route path="/placeAdinputs/car" component={PlaceAdCars} />
                <Route path="/placeAdinputs/realState" component={PlaceAdRealState} />
                <Route path="/placeAdinputs/jobs" component={PlaceAdJobs} />
                <Route path="/viewAd/:id" component={ViewMyAd} />
                <Route path="/category/:id/subcategory/:subId" component={Category} />
                <Route path="/newDesgin/:id" component={NewDesgin} />
                <Route path="*" component={NotFound} />
              </Switch>
            <Footer />
        </BrowserRouter>
        </PersistGate>
      </Provider>

    );
  }
}


export default  App