import React, { Component } from 'react';
import './pagination.css';
import { connect } from 'react-redux';

class Pagination extends Component {

    state = {
        arr: [1, 2, 3, 4, 5, 6],
    }

    getArr = (currentPage, limit, pageCount) => {

        let factor = parseInt(currentPage / limit);
        if (currentPage % limit === 0) factor -= 1;  // if the currentPage == limit we should reduce it by one
        factor *= limit;

        let arr = [];
        for (let i = 1; i <= limit; i += 1) {
            let res = i + factor;
            if (res <= pageCount) arr.push(res);
        }

        return arr;
    }

    componentDidMount() {
       this.setState({ arr: this.getArr(this.props.currentPage, this.props.bullets ,this.props.totalPages ) }) ; 
    }

    componentDidUpdate(prevProps){
        if( prevProps.currentPage !== this.props.currentPage )
             this.setState({ arr: this.getArr(this.props.currentPage, this.props.bullets ,this.props.totalPages ) }) ;
    }

    render() {

        return (
            <div className="pagination-wrapper" >
                <div className="d-flex" >
                    <ul className="pagination-list" >

                        {this.props.currentPage > 1 &&
                            <li onClick={this.props.onPrevClick} >
                                <i className={"fa fa-arrow-"+(this.props.appLanguage === "en" ? "left" : "right" )} aria-hidden="true"></i>
                            </li>
                        }

                        {this.state.arr.map((item, index) =>
                            <li key={index} onClick={() => this.props.onItemClick(item)}
                                className={this.props.currentPage === item ? "active-bullet" : ""} >
                                {item}
                            </li>
                        )}

                        {this.props.currentPage < this.props.totalPages &&  
                        <li onClick={this.props.onNextClick} >
                            <i className={"fa fa-arrow-"+(this.props.appLanguage === "en" ? "right" : "left") } aria-hidden="true"></i>
                        </li>
                        }

                    </ul>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
    }
}
export default connect(mapStateToProps, null)(Pagination);