import React, { Component } from 'react';
import './viewAd.css';
// import { Link } from 'react-router-dom'
// import face from '../../../assets/images/face.png'
// import phone from '../../../assets/images/phone.png'
// import email from '../../../assets/images/email.png'
import String from '../../../assets/locals';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { endPoint } from '../../../assets/apiEndPoint';
import axios from 'axios';
import { bindActionCreators } from 'redux';
import { addToFavourites, removeFromFav, followUser, unfollowUser } from '../../../redux/Actions/userActions';
import ImageSlider from './imageSlider/imageSlider';
import CategoryCard from '../../category/categoryCard';
import ImagePopup from './imageSlider/imagePopup';

class ViewMyAd extends Component {

    state = {
        adDetails: null,
        images: null,
        currentIamge: 0,
        sliderAds: [],
        showPopUp: false
    }

    fetchAdData = (id) => {

        let uri = `${endPoint}ads/${id}`;
        axios.get(uri)
            .then(res => {
                console.log("data  ", res.data)
                this.setState({ adDetails: res.data, images: res.data.img, currentIamge: 0 });
                this.fetchAds(res.data.category.id);
            })
            .catch(err => console.log("err", err));
    }

    fetchAds = (id) => {
        let uri = `${endPoint}ads?category=${id}`;
        axios.get(uri)
            .then(response => {
                let sliderAds = response.data.data.filter(element => element.id !== this.state.adDetails.id);
                this.setState({ sliderAds })
            })
            .catch(err => console.log("err", err));
    }

    componentDidMount() {
        this.fetchAdData(this.props.match.params.id);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id !== this.props.match.params.id)
            this.fetchAdData(this.props.match.params.id);
    }

    componentWillUnmount() {
        this.setState({
            adDetails: null,
            images: null,
            currentIamge: 0,
            sliderAds: [],
            showPopUp: false
        });
    }

    popupToggle = () => {
        this.setState({ showPopUp: !this.state.showPopUp });
    }

    getDate(date) {
        let mydate = new Date(date);
        return mydate.toDateString();
    }

    inFavs(id) {
        if (!this.props.isLogged) return false;

        let favs = this.props.currentUser.favourite;
        for (let i = 0; i < favs.length; i++)
            if (favs[i] === id) return true;

        return false;
    }

    inFollowing(id) {
        let { following } = this.props.currentUser;
        for (let i = 0; i < following.length; i++)
            if (following[i] === id) return true;

        return false;
    }

    handleFavReducer = (id) => {
        if (!this.inFavs(id)) this.props.addToFavourites(id);
        else this.props.removeFromFav(id);
    }

    handleFollowingReducer = (id) => {
        if (!this.inFollowing(id)) this.props.followUser(id);
        else this.props.unfollowUser(id);
    }


    render() {
        String.setLanguage(this.props.appLanguage);
        let { adDetails } = this.state;

        return (
            <div className="viewAd">

                {this.state.showPopUp && <ImagePopup popupToggle={this.popupToggle} images={adDetails.img} />}

                <div className="container">
                    {this.state.adDetails &&
                        <div>
                            <div className="row mt-4 justify-content-between " >

                                <div className="col-sm-12 col-md-6 col-lg-4" >
                                    <h4 className="d-flex justify-content-start adDetails-title-handle " > <strong> {adDetails.title} </strong> </h4>
                                </div>

                                <div className="col-sm-12 col-md-6 col-lg-3" >

                                    {!adDetails.price &&
                                        <p className="card-ad-price" >
                                            {String.cardPriceFrom} : {adDetails.priceFrom} - {String.cardPriceTo} : {adDetails.priceTo} {adDetails.country.currency}
                                        </p>
                                    }

                                    {adDetails.price &&
                                        <p className="card-ad-price"  style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end' }} >
                                             {String.adPrice} : 
                                             <span  style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-end':'flex-start' }}>{adDetails.price +" "+ adDetails.country.currency}</span>
                                        </p>
                                    }

                                </div>

                                <div className="col-sm-12 col-md-12 col-lg-5 d-flex justify-content-end" >
                                    {this.props.isLogged && parseInt(adDetails.owner.id) !== parseInt(this.props.currentUser.id) &&
                                        <button className="btn ad-header-buttons mb-3" onClick={() => this.handleFavReducer(adDetails.id)} >
                                            <i className="fa fa-heart" style={{ color: this.inFavs(adDetails.id) ? "red" : 'white' }}  ></i>
                                            <span>{String.adDetailsSave}</span>
                                        </button>
                                    }

                                    {this.props.isLogged && parseInt(adDetails.owner.id) !== parseInt(this.props.currentUser.id) &&
                                        <button style={{
                                            backgroundColor: this.inFollowing(adDetails.owner.id) ? "red" : "#f1f1f1",
                                            color: this.inFollowing(adDetails.owner.id) ? "white" : "black"
                                        }}
                                            className="btn ad-header-buttons mb-3"
                                            onClick={() => this.handleFollowingReducer(adDetails.owner.id)}  >
                                            {this.inFollowing(adDetails.owner.id) ? String.adDetailsUnFollow : String.adDetailsFollow}
                                        </button>
                                    }

                                    {this.props.isLogged && parseInt(adDetails.owner.id) !== parseInt(this.props.currentUser.id) &&
                                        <Link to={`/newDesgin/${adDetails.owner.id}`} className="btn ad-header-buttons mb-3"  >
                                            <i className="fa fa-commenting"></i>
                                            <span> {String.adDetailsChat}</span>
                                        </Link>
                                    }

                                </div>
                            </div>

                            <div className="row d-flex my-4" >
                                <ul className="breadcrumb w-100 ">
                                    <li>
                                        <Link to="/" style={{color:'#679C8A'}}>
                                            {String.home}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`/category/${adDetails.category.id}/subcategory/all`} style={{color:'#679C8A'}}>
                                            {this.props.appLanguage === "en" ? adDetails.category.name : adDetails.category.arabicName}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`/category/${adDetails.category.id}/subcategory/${adDetails.subCategory.id}`} style={{color:'#679C8A'}}>
                                            {this.props.appLanguage === "en" ? adDetails.subCategory.name : adDetails.subCategory.arabicName}
                                        </Link>
                                    </li>
                                    <li>{adDetails.title}</li>
                                </ul>
                            </div>

                            <div className="row justify-content-left " >
                                <ImageSlider popupToggle={this.popupToggle} images={adDetails.img} />
                            </div>

                            {/* DETAILS DIV */}
                            <div className="row" >
                                <div className="col-sm-12 col-lg-8  mt-5 mb-3 pt-3" >
                                    <h4 className="d-flex" > <strong> {String.description} </strong> </h4>
                                    <p className="d-flex" style={{ color: "#999" }} > {adDetails.description} </p>
                                </div>

                                <div className="col-sm-12 col-lg-8 ads-border pt-3 d-flex justify-content-between" >
                                    <h4> <strong>  {String.detials} </strong> </h4>
                                    <p className="labels-text" > {String.adDetailsPostOn} {this.getDate(adDetails.createdAt)} </p>
                                </div>

                                <div className="col-sm-12 col-lg-8 d-flex ads-border pt-3 justify-content-between" >
                                    <p className="ad-props-details" > {String.adDetailsAvailCountry} :</p>
                                    <p className="labels-text">
                                        {this.props.appLanguage === "en" ? adDetails.country.countryName : adDetails.country.arabicName}
                                    </p>
                                </div>

                                <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                    <p className="ad-props-details" > {String.adAddress} </p>
                                    <p className="labels-text" > {adDetails.address} </p>
                                </div>

                                <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                    <p className="ad-props-details">  {String.adDetailsContact}  </p>
                                    <p className="labels-text" > {adDetails.phone} </p>
                                </div>

                                <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                    <p className="ad-props-details" > {String.adDetailsRate} </p>
                                    <div className="d-flex" >
                                        <i className={"fa fa-star " + (adDetails.rate > 0 ? "ad-rate-yellow" : "ad-rate")}  ></i>
                                        <i className={"fa fa-star " + (adDetails.rate > 20 ? "ad-rate-yellow" : "ad-rate")}  ></i>
                                        <i className={"fa fa-star " + (adDetails.rate > 40 ? "ad-rate-yellow" : "ad-rate")}  ></i>
                                        <i className={"fa fa-star " + (adDetails.rate > 60 ? "ad-rate-yellow" : "ad-rate")}  ></i>
                                        <i className={"fa fa-star " + (adDetails.rate > 80 ? "ad-rate-yellow" : "ad-rate")}  ></i>
                                    </div>
                                    {/* <p style={{ color: "#999" }} > {adDetails.rate} </p> */}
                                </div>

                            </div>

                            {/* PUBLISHER DETAILS */}
                            <div className="row " >

                                <div className="col-sm-12 col-lg-8 ads-border pt-3 ads-border  mb-2 " >
                                    <h4 className="d-flex" > <strong> {String.adDetailsAboutPublisher} </strong> </h4>
                                </div>

                                <div className="col-sm-12 col-lg-8 d-flex justify-content-between ads-border pt-3 " >
                                    <p className="ad-props-details" >  {String.userName}  </p>
                                    <p className="labels-text" > {adDetails.owner.username} </p>
                                </div>

                                <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                    <p className="ad-props-details" >  {String.email}  </p>
                                    <p className="labels-text" > {adDetails.email} </p>
                                </div>

                                <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                    <p className="ad-props-details" >  {String.adDetailsJoinedAt} </p>
                                    <p className="labels-text" > {this.getDate(adDetails.owner.createdAt)} </p>
                                </div>

                            </div>

                            {/* CAR DETAIL */}
                            {adDetails.category.type === "MOTOR" &&
                                <div className="row" >
                                    <div className="col-sm-12 col-lg-8 ads-border pt-3 ads-border  mb-2 " >
                                        <h4 className="d-flex" > <strong> {String.adDetailsCarDetails} </strong> </h4>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between ads-border pt-3 " >
                                        <p className="ad-props-details" >  {String.adColor}  </p>
                                        <p className="labels-text" > {this.props.appLanguage === "en" ?adDetails.color.colorName:adDetails.color.arabicColorName} </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                        <p className="ad-props-details" >  {String.year} </p>
                                        <p className="labels-text" > {adDetails.year} </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                        <p className="ad-props-details" >  {String.carBrand} </p>
                                        <p className="labels-text" > {adDetails.brand} </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                        <p className="ad-props-details" >  {String.carModal} </p>
                                        <p className="labels-text" > {adDetails.modal} </p>
                                    </div>

                                   
                                </div>
                            }

                            {/* REAL STATE DEATAIS */}
                            {adDetails.category.type === "REAL-STATE" &&
                                <div className="row" >

                                    <div className="col-sm-12 col-lg-8 ads-border pt-3 ads-border  mb-2 " >
                                        <h4 className="d-flex" > <strong> {String.adDetailsRealState} </strong> </h4>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between ads-border pt-3 " >
                                        <p className="ad-props-details" >  {String.bathsNumber}  </p>
                                        <p className="labels-text" > {adDetails.baths} </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                        <p className="ad-props-details" >  {String.space} </p>
                                        <p className="labels-text" > {adDetails.space} (sqft) </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                        <p className="ad-props-details" >  {String.bedsNumber} </p>
                                        <p className="labels-text" > {adDetails.beds} </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                        <p className="ad-props-details" >  {String.rentalFrequency} </p>
                                        <p className="labels-text" > {adDetails.rental} </p>
                                    </div>
                                </div>
                            }

                            {/* Jop Details */}
                            {adDetails.category.type === "JOPS" &&
                                <div className="row" >

                                    <div className="col-sm-12 col-lg-8 ads-border pt-3 ads-border  mb-2 " >
                                        <h4 className="d-flex" > <strong> {String.adDetailsJop} </strong> </h4>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex flex-column ads-border pt-3" >
                                        <p style={{ textAlign: this.props.appLanguage === "en" ? "left" : "right" }}
                                            className="ad-props-details" >
                                            {String.experience}
                                        </p>
                                        <p className="labels-text" style={{ textAlign: this.props.appLanguage === "en" ? "left" : "right" }}>
                                            {adDetails.experience}
                                        </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex flex-column" >
                                        <p style={{ textAlign: this.props.appLanguage === "en" ? "left" : "right" }}
                                            className="ad-props-details" >
                                            {String.adJopRequirment}
                                        </p>
                                        <p className="labels-text" style={{ textAlign: this.props.appLanguage === "en" ? "left" : "right" }} >
                                            {adDetails.jopRequirements}
                                        </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between " >
                                        <p style={{ textAlign: this.props.appLanguage === "en" ? "left" : "right" }}
                                            className="ad-props-details" >
                                            {String.adjopType}
                                        </p>
                                        <p className="labels-text d-flex" > {adDetails.jopType} </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                        <p className="ad-props-details" >  {String.salarySystem} </p>
                                        <p className="labels-text" > {adDetails.salarySystem} </p>
                                    </div>

                                    <div className="col-sm-12 col-lg-8 d-flex justify-content-between" >
                                        <p className="ad-props-details" >  {String.date} </p>
                                        <p className="labels-text" > {adDetails.date} </p>
                                    </div>

                                </div>
                            }

                        </div>
                    }
                </div>

                {/* CATEGORY SLIDER */}
                <div className="p-4" >
                    {this.state.sliderAds.length > 0 &&
                        <h4 className="d-flex mb-4 " > <strong> {String.realtedSearchs} </strong>  </h4>
                    }
                    <div className="row" >
                        {this.state.sliderAds.length > 0 && this.state.sliderAds.map((element, index) =>
                            index < 4 && <CategoryCard key={index} ads={element} />
                        )}
                    </div>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        favs: state.userReducer.favourites,
        currentUser: state.userReducer.user,
        isLogged: state.userReducer.logged,
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {
        addToFavourites,
        removeFromFav,
        followUser,
        unfollowUser
    },
    dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(ViewMyAd);