import React, { Component } from 'react';
import times from '../../../../assets/images/times.png';
import '../viewAd.css';

class ImagePopup extends Component {

    state = {
        images: this.props.images.length === 0 ? [] : this.props.images,
        currentImage: 0
    }

    handleImageSwap = (index) => {
        if (index < 0) index = 0;
        if (index > this.state.images.length - 1) index = this.state.images.length - 1;
        this.setState({ currentImage: index });
    }

    render() {

        const { images } = this.state;
        let { currentImage } = this.state;

        return (
            <div className="popup-wrapper" >
                <div className="popup-header" >
                    <img src={times} alt="times" onClick={this.props.popupToggle} />
                    <span> {currentImage+1}/{images.length} </span>
                </div>
                <div className="popup-image-show d-flex justify-content-center" >
                    <div className="image-slider-control" >

                        {currentImage > 0 &&
                            <i className="fa fa-arrow-circle-left slider-control-left"
                                onClick={() => this.handleImageSwap(this.state.currentImage - 1)}> </i>
                        }

                        {currentImage < images.length - 1 &&
                            <i className="fa fa-arrow-circle-right slider-control-right"
                                onClick={() => this.handleImageSwap(this.state.currentImage + 1)} ></i>
                        }

                    </div>
                    <img src={images[currentImage]} alt="popups" />
                </div>
            </div>
        )
    }
}
export default ImagePopup
