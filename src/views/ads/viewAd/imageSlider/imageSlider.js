import React, { Component } from 'react';
import './imageSlider.css';

// import one from '../../../../assets/images/1.jpg';
// import two from '../../../../assets/images/2.jpg';
// import three from '../../../../assets/images/3.jpg';
// import four from '../../../../assets/images/4.jpg';

class ImageSlider extends Component {

    state = {
        images: this.props.images.length === 0 ? [] : this.props.images,
        currentImage: 0
    }

    componentDidUpdate(prevProps)
    {
        if( prevProps.images[0] !== this.props.images[0] )
            this.setState({ images : this.props.images }) ;
    }

    

    handleImageSwap = (index) => {
        if (index < 0) index = 0;
        if (index > this.state.images.length - 1) index = this.state.images.length - 1;
        this.setState({ currentImage: index });
    }

    render() {

        return (
            <div className=" slider-wrapper d-flex" >
                <div className="image-slider" >
                    <img className="image-slider-img" onClick={ this.props.popupToggle } src={this.state.images[this.state.currentImage]} alt="product" />
                    <div className="image-slider-control" >
                        {this.state.currentImage > 0 &&
                            <i className="fa fa-arrow-circle-left slider-control-left"
                                onClick={() => this.handleImageSwap(this.state.currentImage - 1)}> </i>
                        }

                        {this.state.currentImage < this.state.images.length - 1 &&
                            <i className="fa fa-arrow-circle-right slider-control-right"
                                onClick={() => this.handleImageSwap(this.state.currentImage + 1)} ></i>
                        }
                    </div>
                </div>
                <div className="image-list" >

                    {this.state.images.length > 1 &&
                        <div>
                            <img className="image-slider-img" src={this.state.images[1]} alt="rest-of-products"
                                 onClick={()=> this.setState({currentImage : 1}) } />
                        </div>
                    }

                    {this.state.images.length > 2 &&
                        <div style={{ margin: "8px 0px" }} >
                            <img className="image-slider-img" src={this.state.images[2]} alt="rest-of-products"
                                 onClick={()=> this.setState({currentImage : 2}) } />
                        </div>
                    }

                    {this.state.images.length > 3 && 
                    <div>
                        <img className="image-slider-img" src={this.state.images[3]} alt="rest-of-products"
                             onClick={ this.props.popupToggle } />
                    </div>
                    }
                </div>
            </div>
        )
    }
}

export default ImageSlider;

