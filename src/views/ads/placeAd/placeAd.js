import React, { Component } from 'react';
import './placeAd.css';
import { Link } from 'react-router-dom';
import String from "../../../assets/locals";
import { connect } from 'react-redux';
import logo from '../../../assets/images/Logogreen.png';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import {withRouter} from 'react-router-dom'

class PlaceAd extends Component {


    // "/placeAdinputs"
    selectLink = (element) => {
        if ( element.type === "MOTOR" ) // CAR
            return "/placeAdinputs/car" ;
        
        else if (element.type === "REAL-STATE") // REAL ESTATE
            return "/placeAdinputs/realState";        

        else if ( element.type === "JOPS" ) // JOBS
            return "/placeAdinputs/jobs" ;

        else 
            return "/placeAdinputs" ; 
    }

    render() {
        String.setLanguage(this.props.appLanguage);

        return (
            <div className="placeads">
                <div className="placeads-header text-center py-1">
                    <div>
                        <Link to="/" >
                            <img width="90" src={logo} alt="prop" className="d-inline-block align-top reg-logo-image" />
                        </Link>
                    </div>
                </div>
                <div className="placeads-body text-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 mx-auto">
                                <div className="row">

                                    <div className="col-lg-12">
                                        <h2 style={{ color: "#2a3234", fontSize: "28px", marginBottom: "10px" }} className="">{String.adTitle}</h2>
                                        <h3 style={{ color: "#7b8385", fontSize: "16px", marginBottom: "3em" }}>{String.adSelect}</h3>
                                    </div>
                                    {
                                        
                                    this.props.categoriesLoading?
                                    <div style={{width:'100%', textAlign:'center'}} >
                                    <Loader
                                    visible={true}
                                    type="Triangle"
                                    color="#679C8A"
                                    height={150}
                                    width={150}
                                    //timeout={20000} //3 secs
                           
                                    />
                                    </div>
                                    :
                                    this.props.categories.map((element, index) => {
                                        return (
                                            <div onClick={()=>{
                                                this.props.history.push( element.type === "MOTOR"?'/placeAdinputs/car': element.type === "REAL-STATE"?"/placeAdinputs/realState":  element.type === "JOPS" ? "/placeAdinputs/jobs" :'/placeAdinputs' ,{data:element})
                                            }} className="col-lg-4 ads-img" key={index}>
                                                {/*<Link to={{
                                                    pathname:
                                                    element.type === "MOTOR"?'/placeAdinputs/car': element.type === "REAL-STATE"?"/placeAdinputs/realState":  element.type === "JOPS" ? "/placeAdinputs/jobs" :'/placeAdinputs' ,
                                                    data:element
                                                }}
                                            */}
                                                
                                                
                                                    <div style={{ background: index > 2 && (index <= 5 || index > 8) ? "#C7BE73" : "#679C8A" }}>
                                                        <img src={element.img} alt="logo" />
                                                        <h6>{this.props.appLanguage === "ar" ? element.arabicName : element.name}</h6>
                                                    </div>
                                               
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        categories: state.headerReducer.categories,
        categoriesLoading: state.headerReducer.categoriesLoading,
    }
}

export default withRouter( connect(mapStateToProps, null)(PlaceAd) ); 