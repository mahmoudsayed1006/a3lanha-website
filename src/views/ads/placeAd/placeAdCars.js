import React, { Component } from 'react';
import './placeAdinputs.css';
import { Link, Redirect } from 'react-router-dom';
import String from "../../../assets/locals";
import { connect } from 'react-redux';
import logo from '../../../assets/images/Logogreen.png';
import { endPoint, localStorageUserToken } from '../../../assets/apiEndPoint';
import axios from 'axios';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'

class PlaceAdCars extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            images: [],
            price: "",
            address: "",
            email: "",
            phone: "",
            color: "",
            usage: -1,
            year: "",
            brand : "" ,
            mileage:'',
            category: 18,
            subCategory: null,
            country: null,
            participate: null,
            allCountries: this.props.countries,
            allBrands: this.props.brands,
            allCeties: null,
            allModels: null,
            selectedCategoryChildren: null,
            selectedCategory: this.props.location.state.data.id,
            selectedSubCategory: this.props.location.state.data.child[0].id,
            selectedCountry: -1,
            selectedCity: -1,
            selectedBrand: -1,
            selectedModel: -1,
            errors: {},
            redirectToHome: false,
            colors:[],
            addAdsLoad:false,
        };
    }

    componentDidMount() {
        let cat = this.props.categories.filter(element => parseInt(element.id) === 18);
        this.setState({ selectedCategoryChildren: cat.length > 0 ? cat[0].child : [] });
        this.fetchBrands();
        this.fetchCountries();
        this.getColors()
        console.log(this.props.categories)
    }

    getColors = () =>{
        axios.get(`${endPoint}/color`)
        .then(response=>{
            console.log("colors   ",response.data)
            this.setState({colors:response.data.data,color:response.data.data[0].id})
        })
        .catch(error=>{
            console.log("error   ",error.response)
        })
    }
   

    fetchCountryCity = (countryIndex) => {
        let uri = `${endPoint}countries/${this.state.allCountries[countryIndex].id}/cities`;
        axios.get(uri)
            .then(response => {
                this.setState({ allCeties: response.data.data });
            })
            .catch(err => console.log("err", err));
    }
    fetchCountries = () => {
        let uri = `${endPoint}countries`;
        axios.get(uri)
            .then(response => {
                this.setState({ allCountries: response.data.data });
            })
            .catch(err => console.log("err", err));
    }
    fetchBrands = () => {
        let uri = `${endPoint}brand`;
        axios.get(uri)
            .then(response => {
                this.setState({ allBrands: response.data.data });
            })
            .catch(err => console.log("err", err));
    }
    fetchModels = (brandIndex) => {
        let uri = `${endPoint}model/${this.state.allBrands[brandIndex].id}/brands`;
        axios.get(uri)
            .then(response => {
                this.setState({ allModels: response.data });
                console.log(response.data)
            })
            .catch(err => console.log("err", err));
    }

    priceValidation = (price) => {

        let firstDot = price.indexOf(".");
        let lastDot = price.lastIndexOf(".");
        if (firstDot !== lastDot) return false;
        if (price === '.') return false;

        for (let i = 0; i < price.length; i++)
            if (price[i] !== '.')
                if (parseInt(price[i]) >= 0 && parseInt(price[i]) <= 9) continue;
                else return false;
        return true;
    }

    yearVaildation = () => {
        let { year } = this.state;

        if (year.length > 4) return false;

        for (let i = 0; i < year.length; i++)
            if (year[i] < '0' || year[i] > '9') return false;

        if (parseInt(year) < 1970 || parseInt(year) > parseInt(new Date().getFullYear())) return false;

        return true;
    }

    handleValidation = () => {
        let errors = {};
        let validForm = true;

        if (!this.state.description || this.state.description.trim() === "") {
            validForm = false;
            errors.description = String.adDescriptionRequired;
            console.log('des')
        }

        if (!this.state.images || this.state.images.length <= 0) {
            validForm = false;
            errors.images = String.adIamgeError
            console.log('img')
        }

        if (!this.state.price) {
            validForm = false;
            errors.price = String.priceRequired;
            console.log('price')
        }
        else if (!this.priceValidation(this.state.price)) {
            validForm = false;
            errors.price = String.priceInvalid;
            console.log('price')
        }
        else if (parseFloat(this.state.price) === 0) {
            validForm = false;
            errors.price = String.priceEqualsZero;
            console.log('price')
        }

        if (!this.state.address || this.state.address.trim() === "") {
            validForm = false;
            errors.address = String.adAddressError
            console.log('address')
        }

        if (!this.state.title || this.state.title.trim() === "") {
            validForm = false;
            errors.title = String.adAddressError
            console.log('title')
        }

        if (!this.state.email || this.state.email.trim() === "") {
            validForm = false;
            errors.email = String.emailRequired;
            console.log('email')
        }
        else if (typeof this.state.email !== "undefined") {
            let lastAtPos = this.state.email.lastIndexOf('@');
            let lastDotPos = this.state.email.lastIndexOf('.');
            console.log('email')

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.email.indexOf('@@') === -1 && lastDotPos > 2 && (this.state.email.length - lastDotPos) > 2)) {
                validForm = false;
                errors.email = String.invaildEmail;
                console.log('email')
            }
        }

        if (!this.state.phone) {
            validForm = false;
            errors.phone = String.phoneRequired
            console.log('phpne')
        }
        else if (typeof this.state.phone !== "undefined") {
            if (!this.state.phone.match(/^[0-9]+$/)) {
                validForm = false;
                errors.phone = String.phoneNumbersOnly;
                console.log('phone')
            }
        }

        if (this.state.color==-1) {
            validForm = false;
            errors.color = String.colorRequired;
            console.log('color')
        }
        if (!this.state.mileage || this.state.mileage.trim() === "") {
            validForm = false;
            errors.mileage = String.mileageRequired;
            console.log('mileage')
        }

       

        if (this.state.selectedCountry === -1) {
            validForm = false;
            errors.country = String.countryRequired;
            console.log('country')
        }

        if (this.state.selectedCity === -1) {
            validForm = false;
            errors.city = String.cityRequired
            console.log('city')
        }
        if (this.state.selectedBrand === -1) {
            validForm = false;
            errors.brand = String.brandRequired;
            console.log('brand')
        }

        if (this.state.selectedModel === -1) {
            validForm = false;
            errors.model = String.modelRequired
            console.log('modal')
        }

        if (this.state.selectedSubCategory === -1) {
            validForm = false;
            errors.subCategory = String.adCategoryError;
            console.log('sun cat')
        }

        if (this.state.usage === -1) {
            validForm = false;
            errors.usage = String.usageRequired;
            console.log('usages')
        }

        if (!this.state.year || this.state.year.trim() === "") {
            validForm = false;
            errors.year = String.caryearYear;
            console.log('year')
        }
        else if (!this.yearVaildation()) {
            validForm = false;
            errors.year = String.caryearYearError;
            console.log('year')
        }

        this.setState({ errors });
        return validForm;

    }

    handleInputsChanges = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    uploadImage = (e) => {
        if (e.target.files[0]) {
            let { images } = this.state;
            images.push(e.target.files[0]);
            this.setState({ images });
        }
    }

    deleteImage = (index) => {
        let { images } = this.state;
        images = images.filter((element, imgIndex) => index !== imgIndex);
        this.setState({ images });
    }

    apiAdYourAd = () => {
        let uri = `${endPoint}ads`;

        let data = new FormData();

        data.append("description", this.state.description);
        data.append("price", this.state.price);
        data.append("address", this.state.address);
        data.append("email", this.state.email);
        data.append("phone", this.state.phone);
        data.append("title", this.state.title);
        data.append("category", this.state.selectedCategory);
        data.append("subCategory", this.state.selectedSubCategory );
        data.append("mileage", this.state.mileage);
        data.append("year", this.state.year);
        data.append("color", this.state.color);

      
        if (this.state.selectedCountry !== -1)
            data.append("country", this.state.allCountries[this.state.selectedCountry].id);

        if (this.state.selectedCity !== -1)
            data.append("city", this.state.allCeties[this.state.selectedCity].id);

        if (this.state.selectedBrand !== -1)
            data.append("brand", this.state.allBrands[this.state.selectedBrand].brandname);

        if (this.state.selectedModel !== -1)
            data.append("modal", this.state.allModels[this.state.selectedModel].modelname);

        if (this.state.usage !== -1)
            data.append("usage", this.state.usage);

        for (let i = 0; i < this.state.images.length; i++)
            data.append("img", this.state.images[i]);
            this.setState({addAdsLoad:true})
        axios.post(uri, data, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(localStorageUserToken)}`
            }
        })
            .then(res => this.setState({addAdsLoad:false, redirectToHome: true }))
            .catch(err => {
                this.setState({addAdsLoad:false})
                alert(String.someThingWrong)
            }  );
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.handleValidation())
            this.apiAdYourAd();
    }

    
    MyVerticallyCenteredModal = () => {
        return (
            <div style={{zIndex:2000, backgroundColor:'rgba(0,0,0,0.5)', position:'fixed',width:'100%',height:'100%', top:0, display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}} >
            <Loader
            visible={true}
            type="Triangle"
            color="#679C8A"
            height={150}
            width={150}                    
            />
            <spa>Wait...</spa>
         </div>
        );
      }


    render() {
        String.setLanguage(this.props.appLanguage);
        
        return (
            <div className="placeAdinputs" >

                {this.state.redirectToHome && <Redirect to="/" />}

                <div className="placeAdinputs-header text-center py-1">
                    <div>
                        <Link to="/" >
                            <img width="90" src={logo} alt="prop" className="d-inline-block align-top reg-logo-image" />
                        </Link>
                    </div>
                </div>

                <div className="container" >

                    <div className="row" >

                        <div className="col-sm-12 col-lg-6 mb-4" >
                            <div className="form-group">

                                <input placeholder={String.addTitle} type="text" className="form-control" name="title" value={this.state.title}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.title.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.title}</span>
                                </p>}
                            </div>

                            <div className="form-group">
                                <textarea placeholder={String.description} className="form-control" rows="5" name="description" value={this.state.description}
                                    onChange={this.handleInputsChanges} />

                                {!this.state.description.replace( /\s/g, '').length&&

                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.description}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group" >
                                <input type="email" className="form-control" name="email" value={this.state.email} placeholder={String.email}
                                    onChange={this.handleInputsChanges} />

                                {!this.state.email.replace( /\s/g, '').length&& 

                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.email}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group">
                                <input type="text" className="form-control" name="address" value={this.state.address} placeholder={String.adAddress}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.address.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.address}</span>
                                </p> 
                                }
                            </div>

                            <div className="form-group">
                                <input placeholder={String.adPrice} type="text" className="form-control" name="price" value={this.state.price}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.price.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.price}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group" >
                                <input type="text" className="form-control" placeholder={String.phone} name="phone" value={this.state.phone}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.phone.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.phone}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group"  >
                                {/*<input type="text" className="form-control" placeholder={String.adColor} name="color" value={this.state.color}
                                    onChange={this.handleInputsChanges} />
                            */}
                                <select className="form-control" value={this.state.color}
                                onChange={(e) => this.setState({ color: e.target.value })} 
                                >
                                    {this.state.colors.map((item, index) =>
                                    <option key={index} value={item.id} >
                                    {this.props.appLanguage === "en" ? item.colorName : item.arabicColorName}
                                    </option>
                                    )
                                    }
                                </select>
                                {this.state.color==-1&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.color}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group"  >
                                <input type="text" className="form-control" placeholder={String.year} name="year" value={this.state.year}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.year.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.year}</span>
                                </p>
                                }
                            </div>
                            <div className="form-group"  >
                                <input type="text" className="form-control" placeholder={String.mileage} name="mileage" value={this.state.mileage}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.mileage.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.mileage}</span>
                            </p> 
                            }
                            </div>

                           {/* <div className="form-group"  >
                                <input type="text" className="form-control" placeholder={String.carBrand} name="brand" value={this.state.brand}
                                    onChange={this.handleInputsChanges} />

                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.brand}</span>
                                </p>
        </div>*/}

                            <div className="form-group" >
                                <select className="form-control" value={this.state.usage}
                                    onChange={(e) => this.setState({ usage: e.target.value })} >

                                    <option value={-1} disabled > {String.adCarState} </option>
                                    <option value={String.realEstateNew} > {String.realEstateNew} </option>
                                    <option value={String.realEstateUsed} > {String.realEstateUsed} </option>

                                </select>
                                {this.state.usage==-1&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.usage}</span>
                                </p>
                                }
                            </div>
                            {/* SELECT brand AND model */}
                            <div className="row" >
                                <div className="col-sm-12 col-lg-6 mb-2 " >

                                    <div className="form-group" >
                                        <select className="form-control" value={this.state.selectedBrand}
                                            onChange={(e) => {
                                                this.setState({ selectedBrand: e.target.value, selectedModel: -1 });
                                                this.fetchModels(e.target.value)
                                            }} >

                                            <option value={-1} disabled > {String.brand} </option>
                                            {this.state.allBrands && this.state.allBrands.map((item, index) =>
                                                <option key={index} value={index} >
                                                    {this.props.appLanguage === "en" ? item.brandname : item.brandname}
                                                </option>
                                            )}

                                        </select>
                                        {this.state.selectedBrand==-1&&
                                        <p className="d-flex" >
                                            <span style={{ color: "red" }}>{this.state.errors.brand}</span>
                                        </p>
                                        }
                                    </div>

                                </div>

                                <div className="col-sm-12 col-lg-6 mb-2 " >

                                    <div className="form-group" >
                                        <select className="form-control" value={this.state.selectedModel}
                                            onChange={(e) => this.setState({ selectedModel: e.target.value })} >

                                            <option value={-1} disabled > {String.selectModel} </option>
                                            {this.state.allModels && this.state.allModels.map((item, index) =>
                                                <option key={index} value={index} >
                                                    {this.props.appLanguage === "en" ? item.modelname : item.modelname}
                                                </option>
                                            )}

                                        </select>
                                        {this.state.selectedModel==-1&&
                                        <p className="d-flex" >
                                            <span style={{ color: "red" }}>{this.state.errors.model}</span>
                                        </p>
                                        }
                                    </div>

                                </div>

                            </div>
                            {/* SELECT COUNTRY AND CITY */}
                            <div className="row" >
                                <div className="col-sm-12 col-lg-6 mb-2 " >

                                    <div className="form-group" >
                                        <select className="form-control" value={this.state.selectedCountry}
                                            onChange={(e) => {
                                                this.setState({ selectedCountry: e.target.value, selectedCity: -1 });
                                                this.fetchCountryCity(e.target.value)
                                            }} >

                                            <option value={-1} disabled > {String.country} </option>
                                            {this.state.allCountries && this.state.allCountries.map((item, index) =>
                                                <option key={index} value={index} >
                                                    {this.props.appLanguage === "en" ? item.countryName : item.arabicName}
                                                </option>
                                            )}

                                        </select>
                                        {this.state.selectedCountry==-1&&
                                        <p className="d-flex" >
                                            <span style={{ color: "red" }}>{this.state.errors.country}</span>
                                        </p>
                                        }
                                    </div>

                                </div>

                                <div className="col-sm-12 col-lg-6 mb-2 " >

                                    <div className="form-group" >
                                        <select className="form-control" value={this.state.selectedCity}
                                            onChange={(e) => this.setState({ selectedCity: e.target.value })} >

                                            <option value={-1} disabled > {String.selectCity} </option>
                                            {this.state.allCeties && this.state.allCeties.map((item, index) =>
                                                <option key={index} value={index} >
                                                    {this.props.appLanguage === "en" ? item.cityName : item.arabicName}
                                                </option>
                                            )}

                                        </select>
                                        {this.state.selectedCity==-1&&
                                        <p className="d-flex" >
                                            <span style={{ color: "red" }}>{this.state.errors.city}</span>
                                        </p>
                                        }
                                    </div>

                                </div>

                            </div>


                           {/* SELECT SUB CATEGORY AND SUB CATEGORY */}
                           <div className="row" >

                        <div className="col-sm-12 col-lg-6 mb-2 " >
                            <select className="form-control" value={this.state.selectedCategory} >
                                <option value={this.state.selectedCategory} defaultValue disabled>{this.props.appLanguage === "en" ? this.props.location.state.data.name:this.props.location.state.data.arabicName }  </option>
                                
                            </select>
                            {this.state.selectedCategory==-1&&
                            <p className="d-flex" >
                                <span style={{ color: "red" }}>{this.state.errors.category}</span>
                            </p>
                            }
                        </div>

                        <div className="col-sm-12 col-lg-6 mb-2 " >

                            <select className="form-control" onChange={(e) => this.setState({ selectedSubCategory: e.target.value })}
                                value={this.state.selectedSubCategory} >

                                <option value={-1} defaultValue >  {String.adSubCategory} </option>
                                {
                                    this.props.location.state.data.child.map((item, index) =>

                                        <option key={index} value={item.id} >
                                            {this.props.appLanguage === "en" ? item.name : item.arabicName}
                                        </option>
                                    )}
                            </select>
                            {this.state.selectedSubCategory==-1&&
                            <p className="d-flex" >
                                <span style={{ color: "red" }}>{this.state.errors.subCategory}</span>
                            </p>
                            }
                        </div>

                        </div>


                        </div>

                        {/* IMAGE UPLOADER */}
                        <div className="col-sm-12 col-lg-6 mb-4" >
                            <button style={{backgroundColor:"#679C8A",borderColor:'#679C8A'}} className="btn btn-block btn-success place-input-button" >
                                {String.uploadIamge}
                                <input type="file" accept="image/*" onChange={this.uploadImage} />
                            </button>
                            {this.state.images.length==0&&
                            <p className="d-flex" >
                                <span style={{ color: "red" }}>{this.state.errors.images}</span>
                            </p>
                            }
                            

                            <div className="row justify-content-start" >
                                {this.state.images && this.state.images.map((element, index) =>
                                    <div key={index} className="placeAdd-img-wrapper">
                                        <button className="btn" style={{ padding: ".375rem 4px" }} value={index} onClick={() => this.deleteImage(index)} >
                                            <i className="fa fa-times " style={{ fontSize: 10 }} > </i>
                                        </button>
                                        <img src={URL.createObjectURL(element)} alt="product-pics" />
                                    </div>
                                )}
                            </div>
                        </div>

                    </div>

                    <div className="row justify-content-center">
                        <button style={{backgroundColor:"#679C8A",borderColor:'#679C8A'}} className="btn btn-block btn-success col-sm-8 col-md-6 mx-auto " onClick={this.handleSubmit} > {String.adSubmit} </button>
                    </div>

                </div>
                {this.state.addAdsLoad&&
                this.MyVerticallyCenteredModal()
               }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        categories: state.headerReducer.categories,
        countries: state.headerReducer.countries
    }
}

export default connect(mapStateToProps, null)(PlaceAdCars);