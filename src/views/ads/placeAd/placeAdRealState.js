import React, { Component } from 'react';
import './placeAdinputs.css';
import { Link, Redirect } from 'react-router-dom';
import String from "../../../assets/locals";
import { connect } from 'react-redux';
import logo from '../../../assets/images/Logogreen.png';
import { endPoint, localStorageUserToken } from '../../../assets/apiEndPoint';
import axios from 'axios';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'

class PlaceAdCars extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            images: [],
            address: "",
            email: "",
            phone: "",
            price: "",
            beds: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            baths: [1, 2, 3, 4, 5],
            space: "",
            rental: -1,

            bedsNumber: -1,
            bathsNumber: -1,

            category: 23,
            subCategory: null,
            country: null,
            participate: null,
            allCountries: this.props.countries,
            allCeties: null,
            selectedCategoryChildren: null,
            selectedCategory: this.props.location.state.data.id,
            selectedSubCategory: this.props.location.state.data.child[0].id,
            selectedCountry: -1,
            selectedCity: -1,
            errors: {},
            redirectToHome: false,
            addAdsLoad:false,
        };
    }

    componentDidMount() {
        let cat = this.props.categories.filter(element => parseInt(element.id) === 23);
        this.setState({ selectedCategoryChildren: cat.length > 0 ? cat[0].child : [] });
    }

    fetchCountryCity = (countryIndex) => {
        let uri = `${endPoint}countries/${this.state.allCountries[countryIndex].id}/cities`;
        axios.get(uri)
            .then(response => {
                this.setState({ allCeties: response.data.data });
            })
            .catch(err => console.log("err", err));
    }

    priceValidation = (price) => {

        let firstDot = price.indexOf(".");
        let lastDot = price.lastIndexOf(".");
        if (firstDot !== lastDot) return false;
        if (price === '.') return false;

        for (let i = 0; i < price.length; i++)
            if (price[i] !== '.')
                if (parseInt(price[i]) >= 0 && parseInt(price[i]) <= 9) continue;
                else return false;
        return true;
    }

    spaceValidation = (number) => {
        for (let i = 0; i < number.length; i++)
            if (number[i] < '0' || number[i] > '9')
                return false;
        return true;
    }

    handleValidation = () => {
        let errors = {};
        let validForm = true;

        if (!this.state.description || this.state.description.trim() === "") {
            validForm = false;
            errors.description = String.adDescriptionRequired;
        }

        if (!this.state.images || this.state.images.length <= 0) {
            validForm = false;
            errors.images = String.adIamgeError
        }

        if (!this.state.address || this.state.address.trim() === "") {
            validForm = false;
            errors.address = String.adAddressError
        }

        if (!this.state.title || this.state.title.trim() === "") {
            validForm = false;
            errors.title = String.adAddressError
        }

        if (!this.state.email || this.state.email.trim() === "") {
            validForm = false;
            errors.email = String.emailRequired;
        }
        else if (typeof this.state.email !== "undefined") {
            let lastAtPos = this.state.email.lastIndexOf('@');
            let lastDotPos = this.state.email.lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.email.indexOf('@@') === -1 && lastDotPos > 2 && (this.state.email.length - lastDotPos) > 2)) {
                validForm = false;
                errors.email = String.invaildEmail;
            }
        }

        if (!this.state.price) {
            validForm = false;
            errors.price = String.priceRequired;
        }
        else if (!this.priceValidation(this.state.price)) {
            validForm = false;
            errors.price = String.priceInvalid;
        }
        else if (parseFloat(this.state.price) === 0) {
            validForm = false;
            errors.price = String.priceEqualsZero;
        }

        if (!this.state.phone) {
            validForm = false;
            errors.phone = String.phoneRequired
        }
        else if (typeof this.state.phone !== "undefined") {
            if (!this.state.phone.match(/^[0-9]+$/)) {
                validForm = false;
                errors.phone = String.phoneNumbersOnly;
            }
        }

        if (this.state.selectedCountry === -1) {
            validForm = false;
            errors.country = String.countryRequired;
        }

        if (this.state.selectedCity === -1) {
            validForm = false;
            errors.city = String.cityRequired
        }

        if (this.state.selectedSubCategory === -1) {
            validForm = false;
            errors.subCategory = String.adCategoryError;
        }

        if (this.state.bedsNumber === -1) {
            validForm = false;
            errors.bedsNumber = String.bedsNumberRequired;
        }

        if (this.state.bathsNumber === -1) {
            validForm = false;
            errors.bathsNumber = String.bathsNumberRequired;
        }


        if (this.state.rental === -1) {
            validForm = false;
            errors.rental = String.rentalRequired;
        }

        if (!this.state.space || !this.spaceValidation(this.state.space)) {
            validForm = false;
            errors.space = String.spaceError;
        }

        this.setState({ errors });
        return validForm;

    }

    handleInputsChanges = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    uploadImage = (e) => {
        if (e.target.files[0]) {
            let { images } = this.state;
            images.push(e.target.files[0]);
            this.setState({ images });
        }
    }

    deleteImage = (index) => {
        let { images } = this.state;
        images = images.filter((element, imgIndex) => index !== imgIndex);
        this.setState({ images });
    }

    apiAdYourAd = () => {
        let uri = `${endPoint}ads`;

        let data = new FormData();

        data.append("description", this.state.description);
        data.append("address", this.state.address);
        data.append("email", this.state.email);
        data.append("phone", this.state.phone);
        data.append("title", this.state.title);
        data.append("category", this.state.selectedCategory);
        data.append("subCategory", this.state.selectedSubCategory );
        data.append("price", this.state.price);
        data.append("space", this.state.space);

        if (this.state.rental !== -1)
            data.append("rental", this.state.rental);

        if (this.state.bedsNumber !== -1)
            data.append("beds", this.state.beds[this.state.bedsNumber]);

        if (this.state.bathsNumber !== -1)
            data.append("baths", this.state.baths[this.state.bathsNumber]);

       

        if (this.state.selectedCountry !== -1)
            data.append("country", this.state.allCountries[this.state.selectedCountry].id);

        if (this.state.selectedCity !== -1)
            data.append("city", this.state.allCeties[this.state.selectedCity].id);

        for (let i = 0; i < this.state.images.length; i++)
            data.append("img", this.state.images[i]);
            this.setState({addAdsLoad:true})
        axios.post(uri, data, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(localStorageUserToken)}`
            }
        })
            .then(res => this.setState({addAdsLoad:false, redirectToHome: true }))
            .catch(err => {
                this.setState({addAdsLoad:false})
                alert(String.someThingWrong)
            });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.handleValidation())
            this.apiAdYourAd();
    }

    
    MyVerticallyCenteredModal = () => {
        return (
            <div style={{zIndex:2000, backgroundColor:'rgba(0,0,0,0.5)', position:'fixed',width:'100%',height:'100%', top:0, display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}} >
            <Loader
            visible={true}
            type="Triangle"
            color="#679C8A"
            height={150}
            width={150}                    
            />
            <spa>Wait...</spa>
         </div>
        );
      }


    render() {
        String.setLanguage(this.props.appLanguage);
        return (
            <div className="placeAdinputs" >

                {this.state.redirectToHome && <Redirect to="/" />}

                <div className="placeAdinputs-header text-center py-1">
                    <div>
                        <Link to="/" >
                            <img width="90" src={logo} alt="prop" className="d-inline-block align-top reg-logo-image" />
                        </Link>
                    </div>
                </div>

                <div className="container" >

                    <div className="row" >

                        <div className="col-sm-12 col-lg-6 mb-4" >
                            <div className="form-group">

                                <input placeholder={String.addTitle} type="text" className="form-control" name="title" value={this.state.title}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.title.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.title}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group">
                                <textarea placeholder={String.description} className="form-control" rows="5" name="description" value={this.state.description}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.description.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.description}</span>
                                </p>
                                }   
                            </div>

                            <div className="form-group" >
                                <input type="email" className="form-control" name="email" value={this.state.email} placeholder={String.email}
                                    onChange={this.handleInputsChanges} />

                                {!this.state.email.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.email}</span>
                                </p>
                                }   
                            </div>

                            <div className="form-group">
                                <input type="text" className="form-control" name="address" value={this.state.address} placeholder={String.adAddress}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.address.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.address}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group" >
                                <input type="text" className="form-control" placeholder={String.phone} name="phone" value={this.state.phone}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.phone.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.phone}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group">
                                <select className="form-control" value={this.state.bedsNumber}
                                    onChange={(e) => this.setState({ bedsNumber: e.target.value })} >

                                    <option value={-1} disabled > {String.bedsNumber} </option>
                                    {this.state.beds.map((item, index) =>
                                        <option value={index} key={index} > {item} </option>
                                    )}
                                </select>
                                {this.state.bedsNumber==-1&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.bedsNumber}</span>
                                </p>
                                }
                                
                            </div>

                            <div className="form-group">
                                <select className="form-control" value={this.state.bathsNumber}
                                    onChange={(e) => this.setState({ bathsNumber: e.target.value })} >

                                    <option value={-1} disabled > {String.bathsNumber} </option>
                                    {this.state.baths.map((item, index) =>
                                        <option value={index} key={index} > {item} </option>
                                    )}
                                </select>
                                {this.state.bathsNumber==-1&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.bathsNumber}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group">
                                <input placeholder={String.adPrice} type="text" className="form-control" name="price" value={this.state.price}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.price.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.price}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group">
                                <select className="form-control" value={this.state.rental}
                                    onChange={(e) => this.setState({ rental: e.target.value })} >

                                    <option value={-1} disabled > {String.rentalFrequency} </option>
                                    <option value={"monthly"}  > {String.rentalYearly} </option>
                                    <option value={"monthly"}  > {String.rentalMonth} </option>
                                    <option value={"weekly"}  > {String.rentalWeek} </option>
                                    <option value={"daily"}  > {String.rentalDaily} </option>

                                </select>
                                {this.state.rental==-1&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.rental}</span>
                                </p>
                                }
                            </div>

                            <div className="form-group">
                                <input type="text" className="form-control" name="space" value={this.state.space} placeholder={String.space}
                                    onChange={this.handleInputsChanges} />
                                {!this.state.space.replace( /\s/g, '').length&&
                                <p className="d-flex" >
                                    <span style={{ color: "red" }}>{this.state.errors.space}</span>
                                </p>
                                }
                            </div>

                            {/* SELECT COUNTRY AND CITY */}
                            <div className="row" >
                                <div className="col-sm-12 col-lg-6 mb-2 " >

                                    <div className="form-group" >
                                        <select className="form-control" value={this.state.selectedCountry}
                                            onChange={(e) => {
                                                this.setState({ selectedCountry: e.target.value, selectedCity: -1 });
                                                this.fetchCountryCity(e.target.value)
                                            }} >

                                            <option value={-1} disabled > {String.country} </option>
                                            {this.state.allCountries && this.state.allCountries.map((item, index) =>
                                                <option key={index} value={index} >
                                                    {this.props.appLanguage === "en" ? item.countryName : item.arabicName}
                                                </option>
                                            )}

                                        </select>
                                        {this.state.selectedCountry==-1&&
                                        <p className="d-flex" >
                                            <span style={{ color: "red" }}>{this.state.errors.country}</span>
                                        </p>
                                        }
                                    </div>

                                </div>

                                <div className="col-sm-12 col-lg-6 mb-2 " >

                                    <div className="form-group" >
                                        <select className="form-control" value={this.state.selectedCity}
                                            onChange={(e) => this.setState({ selectedCity: e.target.value })} >

                                            <option value={-1} disabled > {String.selectCity} </option>
                                            {this.state.allCeties && this.state.allCeties.map((item, index) =>
                                                <option key={index} value={index} >
                                                    {this.props.appLanguage === "en" ? item.cityName : item.arabicName}
                                                </option>
                                            )}

                                        </select>
                                        {this.state.selectedCity==-1&&
                                        <p className="d-flex" >
                                            <span style={{ color: "red" }}>{this.state.errors.city}</span>
                                        </p>
                                        }
                                    </div>

                                </div>

                            </div>

                         {/* SELECT SUB CATEGORY AND SUB CATEGORY */}
                         <div className="row" >

                        <div className="col-sm-12 col-lg-6 mb-2 " >
                            <select className="form-control" value={this.state.selectedCategory} >
                                <option value={this.state.selectedCategory} defaultValue disabled>{this.props.appLanguage === "en" ? this.props.location.state.data.name:this.props.location.state.data.arabicName }  </option>
                                
                            </select>
                            {this.state.selectedCategory==-1&&
                            <p className="d-flex" >
                                <span style={{ color: "red" }}>{this.state.errors.category}</span>
                            </p>
                            }
                        </div>

                        <div className="col-sm-12 col-lg-6 mb-2 " >

                            <select className="form-control" onChange={(e) => this.setState({ selectedSubCategory: e.target.value })}
                                value={this.state.selectedSubCategory} >

                                <option value={-1} defaultValue >  {String.adSubCategory} </option>
                                {
                                    this.props.location.state.data.child.map((item, index) =>

                                        <option key={index} value={item.id} >
                                            {this.props.appLanguage === "en" ? item.name : item.arabicName}
                                        </option>
                                    )}
                            </select>
                            {this.state.selectedSubCategory==-1&&
                            <p className="d-flex" >
                                <span style={{ color: "red" }}>{this.state.errors.subCategory}</span>
                            </p>
                            }
                        </div>

                        </div>


                        </div>


                        
                        {/* IMAGE UPLOADER */}
                        <div className="col-sm-12 col-lg-6 mb-4" >
                            <button style={{backgroundColor:"#679C8A",borderColor:'#679C8A'}} className="btn btn-block btn-success place-input-button" >
                                {String.uploadIamge}
                                <input type="file" accept="image/*" onChange={this.uploadImage} />
                            </button>
                            {this.state.images.length==0&&
                            <p className="d-flex" >
                                <span style={{ color: "red" }}>{this.state.errors.images}</span>
                            </p>
                            }

                            <div className="row justify-content-start" >
                                {this.state.images && this.state.images.map((element, index) =>
                                    <div key={index} className="placeAdd-img-wrapper">
                                        <button className="btn" style={{ padding: ".375rem 4px" }} value={index} onClick={() => this.deleteImage(index)} >
                                            <i className="fa fa-times " style={{ fontSize: 10 }} > </i>
                                        </button>
                                        <img src={URL.createObjectURL(element)} alt="product-pics" />
                                    </div>
                                )}
                            </div>
                        </div>

                    </div>

                    <div className="row justify-content-center">
                        <button style={{backgroundColor:"#679C8A",borderColor:'#679C8A'}} className="btn btn-block btn-success col-sm-8 col-md-6 mx-auto " onClick={this.handleSubmit} > {String.adSubmit} </button>
                    </div>

                    

                </div>
                {this.state.addAdsLoad&&
                this.MyVerticallyCenteredModal()
                }
               

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        categories: state.headerReducer.categories,
        countries: state.headerReducer.countries
    }
}

export default connect(mapStateToProps, null)(PlaceAdCars);