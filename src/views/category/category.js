import React, { Component } from 'react';
import './category.css';
import String from "../../assets/locals";
import { connect } from 'react-redux';
import { endPoint } from "../../assets/apiEndPoint";
import axios from 'axios';
import CategoryCard from './categoryCard';
import Pagination from "../pagination/pagination";
// import { Link } from 'react-router-dom';

class Category extends Component {

    state = {
        categoryAds: null,
        pageCount: 1,
        currentPage: 1,
        openedDropList: -1,
        cities: [],
        selectedCity: this.props.currentCity? this.props.currentCity.id:-1,
        cityText:this.props.currentCity? this.props.appLanguage === "en" ? this.props.currentCity.cityName : this.props.currencurrentCitytCountry.arabicName: String.allCountries,
        extra: {},
        topAds: false,
        selectedCountry:this.props.currentCountry? this.props.currentCountry.id:-1,
        countryText:this.props.currentCountry? this.props.appLanguage === "en" ? this.props.currentCountry.countryName : this.props.currentCountry.arabicName: String.allCountries,
        selectedSubcategory: -1,
        colors: [],
        brands:[],
    }

    fetchCategoryAds = (page, query) => {

        let uri = `${endPoint}ads?category=${this.props.match.params.id}&limit=12&page=${page}`;
        if (query) uri += query;
        axios.get(uri)
            .then(res => {
                this.setState({
                    categoryAds: res.data.data,
                    currentPage: res.data.page,
                    pageCount: res.data.pageCount
                })
            })
            .catch(err => console.log("err", err));
    }

    fetchCities = (countryId) => {
        let uri = `${endPoint}countries/${countryId}/cities`;
        axios.get(uri)
            .then(response => {
                console.log('Cities    ',response.data)
                this.setState({ cities: response.data.data, selectedCity: -1 })
            })
            .catch(err => console.log("err", err));
    }

    getColors = () => {
        let uri = `${endPoint}color`;
        axios.get(uri)
            .then(response =>{
                console.log("Colores   ",response.data.data)
                this.setState({ colors: response.data.data})
            } )
            .catch(err => console.log("err", err));
    }

    getBrands = () => {
        let uri = `${endPoint}brand`;
        axios.get(uri)
            .then(response =>{
                console.log("brands   ",response.data.data)
                this.setState({ brands: response.data.data})
            } )
            .catch(err => console.log("err", err));
    }

    componentWillUnmount() {
        this.setState({
            categoryAds: null,
            extra: {},
            selectedCity: -1,
            selectedCountry: -1,
            selectedSubcategory: -1,
            colors:[]
        });

    }

    //
    componentDidMount() {
        console.log("curreny we user   ",this.props.user)
        this.getColors()
        this.getBrands()

        let filterParameters=`&subCategory=${this.props.match.params.subId}`   
             
        if (this.props.currentCountry){
            this.handleCountryCities(this.props.currentCountry.id);
            filterParameters=`${filterParameters}&country=${this.props.currentCountry.id}`
        } 

        if (this.props.currentCity){
            filterParameters=`${filterParameters}&city=${this.props.currentCity.id}`
        } 
        
       

        if (this.props.match.params.subId === "all") this.fetchCategoryAds(this.state.currentPage);
        else {
            const { id } = this.props.match.params;

            for (let i = 0; i < this.props.categories.length; i++)
                if (this.props.categories[i].id === id) {
                    this.setState({ selectedSubcategory: i });
                    break;
                }
            


            this.fetchCategoryAds(this.state.currentPage, `${filterParameters}`);
  
            
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id !== this.props.match.params.id) {
            this.setState({
                selectedCity: -1,
                selectedCountry: -1,
                selectedSubcategory: -1,
                extra: {}
            });
            this.fetchCategoryAds();
        }

       
    }

    handleSortPrice = () => {
        if (!this.state.sortByPrice) {
            this.setState({ sortByPrice: true });
            this.fetchCategoryAds(this.state.currentPage);
        }
        else {
            this.setState({ sortByPrice: false });
        }
    }

    handleTopAds = () => {
        if (!this.state.topAds) {
            this.setState({ topAds: true });
            this.fetchCategoryAds(this.state.currentPage, "topAds");
        }
        else this.setState({ topAds: false });
    }

    onItemClick = (page) => {
        this.fetchCategoryAds(page);
    }

    onPrevClick = () => {
        this.fetchCategoryAds(this.state.currentPage - 1);
    }

    onNextClick = () => {
        this.fetchCategoryAds(this.state.currentPage + 1);
    }

    handleSubcategories = (index) => {
        this.setState({ subcategories: this.props.categories[index].child });
    }

    handleDropdown = (index) => {
        if (this.state.openedDropList === index)
            this.setState({ openedDropList: -1 });
        else
            this.setState({ openedDropList: index });
    }

    handleCountryCities = (e) => {
        this.fetchCities(e);
    }

    handleFilteration = (key, value) => {
        const { extra } = this.state;
        extra[key] = value;

        let query = ""
        for (let prop in extra) {
            if (extra[prop] !== 'all')
                query += `&${prop}=${extra[prop]}`;
            else delete extra[prop];
        }

        this.fetchCategoryAds(this.state.currentPage, query);
    }

    render() {


        String.setLanguage(this.props.appLanguage);

        const { id } = this.props.match.params;
        const currectCategory = this.props.categories.filter(item => parseInt(item.id) === parseInt(id));
        const subcategories = currectCategory.length === 1 ? currectCategory[0].child : [];

        let bedsOrbath = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        let colors = ["red", "black", "green", "yellow", "white", "blue", "purple"];
        let brands = ["BMW", "Toyota", "Fiat"];

        return (
            <div className="category-card " >

                <div className="p-4">
                    <ul className="nav nav-tabs nav-justified p-0" >

                        {/* SELECT COUNTRY */}
                        <li className="nav-item tab-border" onClick={() => this.handleDropdown(0)} >
                            <div className="d-flex justify-content-around">
                                <span className="mx-2" >
                                  {this.state.countryText}
                                </span>
                                <span className="mx-2" > <i className="arrow" ></i> </span>
                            </div>

                            <div className="category-drop-list"
                                onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                onClick={() => this.setState({ openedDropList: -1 })}
                                style={{ top: "40px", display: this.state.openedDropList === 0 ? "block" : "none" }} >

                                <ul className="nav flex-column p-0 " >
                                    <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                        onClick={() => { this.handleFilteration("country", "all"); this.setState({countryText:String.allCountries, selectedCountry: -1 }) }} >
                                        {String.allCountries}
                                    </li>
                                    {this.props.countries.map((element, index) =>
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item" key={index}
                                            onClick={() => {
                                                this.handleCountryCities(element.id);
                                                this.handleFilteration("country", element.id);
                                                this.setState({countryText:this.props.appLanguage === "en" ? element.countryName : element.arabicName, selectedCountry: index })
                                            }} >
                                            {this.props.appLanguage === "en" ? element.countryName : element.arabicName}
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </li>

                        {/* SELECT PRICE */}
                        <li className="nav-item tab-border" onClick={() => this.handleDropdown(1)} >
                            <div className="d-flex justify-content-around">
                                <span className="mx-2" > {String.adPrice} </span>
                                <span className="mx-2" > <i className="arrow" ></i> </span>
                            </div>

                            <div className="category-drop-list"
                                onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                onClick={() => this.setState({ openedDropList: -1 })}
                                style={{ top: "40px", display: this.state.openedDropList === 1 ? "block" : "none" }} >

                                <ul className="nav flex-column p-0 " >
                                    <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                        onClick={() => this.handleFilteration("sortByPrice", false)} >
                                        {String.priceLowest}
                                    </li>

                                    <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                        onClick={() => this.handleFilteration("sortByPrice", true)} >
                                        {String.priceHightest}
                                    </li>
                                </ul>
                            </div>
                        </li>

                        {/* SELECT CITY */}
                        <li className="nav-item tab-border" onClick={() => this.handleDropdown(2)} >
                            <div className="d-flex justify-content-around">
                                <span className="mx-2" >
                                    {this.state.cityText}
                                    {/*this.state.selectedCity === -1 ? String.allCities :
                                        this.props.appLanguage === "en" ? this.state.cities[this.state.selectedCity].cityName :
                                    this.state.cities[this.state.selectedCity].arabicName*/}
                                </span>
                                <span className="mx-2" > <i className="arrow" ></i> </span>
                            </div>

                            <div className="category-drop-list"
                                onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                onClick={() => this.setState({ openedDropList: -1 })}
                                style={{ top: "40px", display: this.state.openedDropList === 2 ? "block" : "none" }} >

                                <ul className="nav flex-column p-0 " >
                                    <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                        onClick={() => { this.handleFilteration("city", "all"); this.setState({ selectedCity: -1,cityText:String.allCities }) }} >
                                        {String.allCities}
                                    </li>

                                    {this.state.cities && this.state.cities.map((element, index) =>
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => { this.setState({cityText:this.props.appLanguage === "en" ? element.cityName : element.arabicName,  selectedCity: index }); this.handleFilteration("city", element.id) }}
                                            key={index} >
                                            {this.props.appLanguage === "en" ? element.cityName : element.arabicName}
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </li>

                        {/* SELECT SUBCATEGORY */}
                        <li className="nav-item tab-border" onClick={() => this.handleDropdown(3)} >
                            <div className="d-flex justify-content-around">
                                <span className="mx-2" >
                                    {this.state.selectedSubcategory === -1 ? String.allSubcategories :
                                        this.props.appLanguage === "en" ? subcategories[this.state.selectedSubcategory].name :
                                            subcategories[this.state.selectedSubcategory].arabicName}
                                </span>
                                <span className="mx-2" > <i className="arrow" ></i> </span>
                            </div>

                            <div className="category-drop-list"
                                onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                onClick={() => this.setState({ openedDropList: -1 })}
                                style={{ top: "40px", display: this.state.openedDropList === 3 ? "block" : "none" }} >

                                <ul className="nav flex-column p-0 " >
                                    <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                        onClick={() => { this.handleFilteration("subCategory", "all"); this.setState({ selectedSubcategory: -1 }) }} >
                                        {String.adSubCategory}
                                    </li>

                                    {subcategories.map((element, index) =>
                                        <li key={index} className="nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => {
                                                this.handleFilteration("subCategory", element.id);
                                                this.setState({ selectedSubcategory: index })
                                            }} >
                                            {this.props.appLanguage === "en" ? element.name : element.arabicName}
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </li>

                        {/* SELECT BEDS - REAL ESTATE ONLY - 23 */}
                        {currectCategory && currectCategory[0] && currectCategory[0].type === "REAL-STATE" &&
                            <li className="nav-item tab-border" onClick={() => this.handleDropdown(4)} >
                                <div className="d-flex justify-content-around">
                                    <span className="mx-2" >
                                        {String.bedsNumber}
                                    </span>
                                    <span className="mx-2" > <i className="arrow" ></i> </span>
                                </div>

                                <div className="category-drop-list"
                                    onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                    onClick={() => this.setState({ openedDropList: -1 })}
                                    style={{ top: "40px", display: this.state.openedDropList === 4 ? "block" : "none" }} >

                                    <ul className="nav flex-column p-0 " >
                                        {bedsOrbath.map((item, index) =>
                                            <li key={index} value={item} className=" nav-item d-flex px-3 py-2 drop-down-item"
                                                onClick={() => this.handleFilteration("beds", item)}>
                                                {item}
                                            </li>
                                        )}
                                    </ul>
                                </div>
                            </li>
                        }

                        {/* SELECT BATHS  */}
                        {currectCategory && currectCategory[0] && currectCategory[0].type === "REAL-STATE" &&
                            <li className="nav-item tab-border" onClick={() => this.handleDropdown(14)} >
                                <div className="d-flex justify-content-around">
                                    <span className="mx-2" >
                                        {String.bathsNumber}
                                    </span>
                                    <span className="mx-2" > <i className="arrow" ></i> </span>
                                </div>

                                <div className="category-drop-list"
                                    onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                    onClick={() => this.setState({ openedDropList: -1 })}
                                    style={{ top: "40px", display: this.state.openedDropList === 14 ? "block" : "none" }} >

                                    <ul className="nav flex-column p-0 " >
                                        {bedsOrbath.map((item, index) =>
                                            <li key={index} value={item} className=" nav-item d-flex px-3 py-2 drop-down-item"
                                                onClick={() => this.handleFilteration("baths", item)}>
                                                {item}
                                            </li>
                                        )}
                                    </ul>
                                </div>
                            </li>
                        }




                        {/* SELECT RENTAL - REAL ESTATE ONLY - 23 */}
                        {currectCategory && currectCategory[0] && currectCategory[0].type === "REAL-STATE" &&
                            <li className="nav-item tab-border" onClick={() => this.handleDropdown(5)} >
                                <div className="d-flex justify-content-around">
                                    <span className="mx-2" >
                                        {String.rentalFrequency}
                                    </span>
                                    <span className="mx-2" > <i className="arrow" ></i> </span>
                                </div>

                                <div className="category-drop-list"
                                    onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                    onClick={() => this.setState({ openedDropList: -1 })}
                                    style={{ top: "40px", display: this.state.openedDropList === 5 ? "block" : "none" }} >

                                    <ul className="nav flex-column p-0 " >
                                        {String.rentalArray.map((item, index) =>
                                            <li key={index} value={index} className=" nav-item d-flex px-3 py-2 drop-down-item"
                                                onClick={() => this.handleFilteration("rental", String.rentalArray[index])}>
                                                {item}
                                            </li>
                                        )}
                                    </ul>
                                </div>
                            </li>
                        }

                        {/* SELECT USAGE - REAL ESTATE ONLY - 18 */}
                        {currectCategory && currectCategory[0] && currectCategory[0].type === "MOTOR" &&
                            <li className="nav-item tab-border" onClick={() => this.handleDropdown(6)} >
                                <div className="d-flex justify-content-around">
                                    <span className="mx-2" >
                                        {String.realEstateUse}
                                    </span>
                                    <span className="mx-2" > <i className="arrow" ></i> </span>
                                </div>

                                <div className="category-drop-list"
                                    onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                    onClick={() => this.setState({ openedDropList: -1 })}
                                    style={{ top: "40px", display: this.state.openedDropList === 6 ? "block" : "none" }} >

                                    <ul className="nav flex-column p-0 " >
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("usage", "all")}>
                                            {String.all}
                                        </li>
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("usage", "New")}>
                                            {String.realEstateNew}
                                        </li>
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("usage", "used")}>
                                            {String.realEstateUsed}
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        }

                        {/* SELECT COLOR - CARS ONLY - 18 */}
                        {currectCategory && currectCategory[0] && currectCategory[0].type === "MOTOR" &&
                            <li className="nav-item tab-border" onClick={() => this.handleDropdown(7)} >
                                <div className="d-flex justify-content-around">
                                    <span className="mx-2" >
                                        {String.carColor}
                                    </span>
                                    <span className="mx-2" > <i className="arrow" ></i> </span>
                                </div>

                                <div className="category-drop-list"
                                    onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                    onClick={() => this.setState({ openedDropList: -1 })}
                                    style={{ top: "40px", display: this.state.openedDropList === 7 ? "block" : "none" }} >

                                    <ul className="nav flex-column p-0 " >
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("color", "all")} >
                                            {String.all}
                                        </li>

                                        {this.state.colors.map((item, index) =>
                                            <img
                                            onClick={() => this.handleFilteration("color", item.id)}
                                              className=" nav-item d-flex px-3 py-2 drop-down-item" src={item.img}
                                              style={{height:40,zIndex:100}}
                                               />
                                            /*<li style={{ backgroundColor: item, color: item }} key={index}
                                                className=" nav-item d-flex px-3 py-2 drop-down-item"
                                                onClick={() => this.handleFilteration("color", item)}>
                                                {item}
                                            </li>
                                            */ 
                                        )}
                                    </ul>
                                </div>
                            </li>
                        }

                        {/* SELECT BRAND - CARS ONLY - 18 */}
                        {currectCategory && currectCategory[0] && currectCategory[0].type === "MOTOR" &&
                            <li className="nav-item tab-border" onClick={() => this.handleDropdown(8)} >
                                <div className="d-flex justify-content-around">
                                    <span className="mx-2" >
                                        {String.carBrand}
                                    </span>
                                    <span className="mx-2" > <i className="arrow" ></i> </span>
                                </div>

                                <div className="category-drop-list"
                                    onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                    onClick={() => this.setState({ openedDropList: -1 })}
                                    style={{ top: "40px", display: this.state.openedDropList === 8 ? "block" : "none" }} >

                                    <ul className="nav flex-column p-0 " >
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("brand", "all")} >
                                            {String.all}
                                        </li>

                                        {this.state.brands.map((item, index) =>
                                            <li key={index}
                                                className=" nav-item d-flex px-3 py-2 drop-down-item"
                                                onClick={() => this.handleFilteration("brand", item.brandname)}>
                                                {item.brandname}
                                            </li>
                                        )}
                                    </ul>
                                </div>
                            </li>
                        }

                        {/* SELECT SALARY SYSTEM - JOPS ONLY - 19 */}
                        {currectCategory && currectCategory[0] && currectCategory[0].type === "JOPS" &&
                            <li className="nav-item tab-border" onClick={() => this.handleDropdown(9)} >
                                <div className="d-flex justify-content-around">
                                    <span className="mx-2" >
                                        {String.salarySystem}
                                    </span>
                                    <span className="mx-2" > <i className="arrow" ></i> </span>
                                </div>

                                <div className="category-drop-list"
                                    onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                    onClick={() => this.setState({ openedDropList: -1 })}
                                    style={{ top: "40px", display: this.state.openedDropList === 9 ? "block" : "none" }} >

                                    <ul className="nav flex-column p-0 " >
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("salarySystem", "all")} >
                                            {String.all}
                                        </li>

                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("salarySystem", "yearly" )}>
                                            {String.salarySystemYearly}
                                        </li>

                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("salarySystem", "month" )}>
                                            {String.salarySystemMonth}
                                        </li>

                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("salarySystem", "week" )}>
                                            {String.salarySystemWeek}
                                        </li>

                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("salarySystem", "daily" )}>
                                            {String.salarySystemDaily}
                                        </li>

                                       

                                    </ul>
                                </div>
                            </li>
                        }

                        {/* SELECT JOP TYPE - JOPS ONLY - 19 */}
                        {currectCategory && currectCategory[0] && currectCategory[0].type === "JOPS" &&
                            <li className="nav-item tab-border" onClick={() => this.handleDropdown(10)} >
                                <div className="d-flex justify-content-around">
                                    <span className="mx-2" >
                                        {String.adjopType}
                                    </span>
                                    <span className="mx-2" > <i className="arrow" ></i> </span>
                                </div>

                                <div className="category-drop-list"
                                    onMouseLeave={() => this.setState({ openedDropList: -1 })}
                                    onClick={() => this.setState({ openedDropList: -1 })}
                                    style={{ top: "40px", display: this.state.openedDropList === 10 ? "block" : "none" }} >

                                    <ul className="nav flex-column p-0 " >
                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("jopType", "all")} >
                                            {String.all}
                                        </li>

                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("jopType", "Full Time" )}>
                                            {String.adJopFull}
                                        </li>

                                        <li className=" nav-item d-flex px-3 py-2 drop-down-item"
                                            onClick={() => this.handleFilteration("jopType", "Part Time" )}>
                                            {String.adJopPart}
                                        </li>

                                       

                                    </ul>
                                </div>
                            </li>
                        }

                    </ul>

                    {this.state.categoryAds && this.state.categoryAds.length < 1 &&
                        <div className="p-5 m-5 text-center" >
                            <strong>{String.noResultFound}</strong>
                        </div>
                    }

                    {this.state.categoryAds && this.state.categoryAds.length > 0 &&
                        <div>

                            <div className="row pt-4" >
                                {this.state.categoryAds.map((element, index) =>
                                    <CategoryCard key={index} ads={element} />
                                )}
                            </div>
                        </div>
                    }

                    {this.state.pageCount > 1 &&
                        <Pagination totalPages={this.state.pageCount}
                            currentPage={this.state.currentPage}
                            bullets={3}
                            onItemClick={this.onItemClick}
                            onPrevClick={this.onPrevClick}
                            onNextClick={this.onNextClick} />
                    }

                </div>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        countries: state.headerReducer.countries,
        categories: state.headerReducer.categories,
        currentCountry: state.userReducer.currentCountry,
        currentCity: state.userReducer.currentCity,
        user:state.userReducer.user,
    }
}

export default connect(mapStateToProps, null)(Category);