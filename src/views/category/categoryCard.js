import React, { Component } from 'react';
import './category.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import String from '../../assets/locals';
import { addToFavourites, removeFromFav } from '../../redux/Actions/userActions';

class CategoryCard extends Component {

    inFavs(id) {
        if (!this.props.isLogged) return false;

        let favs = this.props.currentUser.favourite;
        for (let i = 0; i < favs.length; i++)
            if (favs[i] === id) return true;

        return false;
    }

    handleFavReducer = (id) => {
        if (!this.inFavs(id)) this.props.addToFavourites(id);
        else this.props.removeFromFav(id);
    }

    render() {

        String.setLanguage(this.props.appLanguage);
        const { ads } = this.props;

        return (
            <div className="col-lg-3 col-md-6 col-sm-12 category-card-body">
                <div className="card-view">

                    <i onClick={() => this.handleFavReducer(ads.id)}
                        className="fav-icon fa fa-heart" style={{ color: this.inFavs(ads.id) ? "red" : 'white' }} ></i>

                    <div className="card-view-img">
                        <Link to={`/viewAd/${ads.id}`} >
                            <img src={ads.img[0]} alt="img" />
                        </Link>
                    </div>
                    <Link to={`/viewAd/${ads.id}`} >
                    <div className="card-view-details">
                        <div className="p-3 card-border-bottom d-flex flex-column "
                            style={{ alignItems: this.props.appLanguage !== "en" ? "end" : "" }} >
                            <h4 className="card-title" > {ads.title} </h4>
                            <p className="card-country" >
                                {this.props.appLanguage === "en" ? `${ads.country.countryName}` : `${ads.country.arabicName}`}
                            </p>
                            { !ads.price &&
                                <p className="card-ad-price" > {String.cardPriceFrom}  {ads.priceFrom} - {String.cardPriceTo} : {ads.priceTo} </p>
                            }

                            { ads.price &&
                                <p className="card-ad-price"  >   {ads.price +" "+ ads.country.currency} </p>
                            }

                            <p className=" card-location"  >
                                <i className="fa fa-map-marker" aria-hidden="true"></i>
                                {' ' + ads.address}
                            </p>

                        </div>
                        <span className="text-center d-block m-3" style={{ color: "#679C8A" }} >{String.Details} </span>
                    </div>
                    </Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        isLogged: state.userReducer.logged,
        favs: state.userReducer.favourites,
        currentUser: state.userReducer.user,
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {
        addToFavourites,
        removeFromFav
    },
    dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(CategoryCard);