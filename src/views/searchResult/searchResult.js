import React, { Component } from 'react';
import { endPoint } from "../../assets/apiEndPoint";
import { connect } from "react-redux";
import String from '../../assets/locals';
import axios from "axios";
import CategoryCard from '../category/categoryCard';
import {localStorageUserSearch} from '../../assets/apiEndPoint' ; 
import './searchResult.css';

class SearchResult extends Component {

    state = {
        result: null,
        pageCount: 1 ,
        currentPage : 1
    }

    fetchUSerSearch = (userSearch, page) => {

        let uri = `${endPoint}ads/search/ads${userSearch}&limit=6&page=${page}`;
        axios.get(uri)
            .then(res => {

                if (this.state.result !== null)
                    this.setState({ result: [...this.state.result, ...res.data.data] ,
                                    currentPage : res.data.page });
                else
                    this.setState({ result: res.data.data , 
                                    pageCount: res.data.pageCount ,
                                    currentPage : res.data.page });
            })
            .catch(err => console.log("err ", err));
    }

    componentDidMount() {
        if(this.props.location.search)
        { 
           this.fetchUSerSearch( this.props.location.search , this.state.currentPage);
           localStorage.setItem(localStorageUserSearch , this.props.location.search) ;
        }
        else if( localStorage.getItem(localStorageUserSearch) )
        {
            this.fetchUSerSearch( localStorage.getItem(localStorageUserSearch)  , this.state.currentPage);
        }

        
    }

    render() {

        String.setLanguage(this.props.appLanguage);

        return (
            <div className="category-card pt-4" id="home-body-opacity" >
                <div className="container" >

                    {this.state.result && this.state.result.length < 1 && 
                        <div className="text-center p-5" >
                            <strong>{String.noResultFound}</strong> 
                        </div>
                    }

                    {this.state.result && this.state.result.length > 0 &&

                        <div>
                            <div className="row" >
                                {this.state.result.map((element, index) =>
                                    <CategoryCard key={index} ads={element} />
                                )}
                            </div>
                            <div className="row justify-content-center my-4 " >
                                {this.state.pageCount > this.state.currentPage &&
                                    <button className="btn btn-success" 
                                            onClick={() => this.fetchUSerSearch(this.props.match.params.word , this.state.currentPage+1 )}>
                                        {String.loadMoreButton}
                                    </button>
                                }
                            </div>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage
    }
}

export default connect(mapStateToProps, null)(SearchResult); 