import React, { Component } from 'react';
import './header.css'
import { Link } from 'react-router-dom';
import logo from '../../assets/images/Logogreen.png'
import String from '../../assets/locals';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeLanguage, fetchCities, fetchCategories, fetchCountries ,setFilterCity } from "../../redux/Actions/headerActions";
import { logOut } from "../../redux/Actions/userActions";
import {localStorageDefaultCountry} from '../../assets/apiEndPoint' ; 
import $ from 'jquery'

class Header extends Component {

    state = {
        redirectTo: null,
        redirect: false,
    }

    redirectToAccount(path) {
        this.setState({ redirectTo: path, redirect: true });
    }

    componentDidMount() {
        this.props.fetchCities( localStorage.getItem(localStorageDefaultCountry) );
        this.props.fetchCategories();
        this.props.fetchCountries();
    }

    opacityHome = () => {
        $(document).ready(function () {
            $(".opacity-navlink").mouseenter(function () {
               // $("#home-body-opacity").addClass("opacit-home")
                $(".home-card").css("border", "0")
            });
            $(".opacity-navlink").mouseleave(function () {
                //$("#home-body-opacity").removeClass("opacit-home")
                $(".home-card").css("border", "1px solid #eef0f1")
            })
        })
    }

    componentDidUpdate() {
        this.opacityHome() ;
    }

    setCurrentCity = (index) => {
        this.props.setFilterCity(index) ;
    }

    // getCurrentCity = () => {

    //     if(this.props.currentCity === "all" )
    //     {
    //         console.log("over here" ) ;
    //         let id = localStorage.getItem(localStorageDefaultCountry) === null ? 1 : localStorage.getItem(localStorageDefaultCountry) 
    //         let country = this.props.countries.filter(item => item.id === id ) ; 
            
    //         if(country.length === 0 ) return null ;
    //         let countryName = this.props.appLanguage === "en" ? country[0].countryName : country[0].arabicName ; 
    //         return String.allCities+'('+countryName+')' ;
           
    //     }
    //     else
    //     {
    //         console.log("here") ; 
    //         let city =  this.props.UAEcities[this.props.currentCity] ; 
    //         return this.props.appLanguage === "en" ? city.cityName : city.arabicName ;
    //     }   
    // }

    render() {
        String.setLanguage(this.props.appLanguage);
        return (
            <div className="view-header">

                {/* start mobile Header */}


                <div className="header-mobile-screen">
                    <nav className="navbar navbar-expand-md bg-dark navbar-dark">
                        <Link to="/">
                            <img src={logo} alt="prop" className="d-inline-block align-top logo-image" />
                        </Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse mt-3" id="collapsibleNavbar">
                            <ul className="navbar-nav">
                                <li style={{ padding: ".5rem 0rem", color: "#fff" }} className="nav-item">
                                    <Link to="#" style={{ color: "#fff" }}>
                                        <span onClick={() => this.props.changeLanguage()} > {String.language} </span>
                                    </Link>
                                </li>
                                <li style={{ padding: ".5rem 0rem", color: "#fff" }} className="nav-item">
                                    <Link to="#" style={{ color: "#fff" }}>
                                        <span> {String.help} </span>
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to={this.props.isLogged?"Chat":"/auth"} > <span>{String.myChats}</span> </Link>
                                </li>

                                <li className="nav-item dropdown">
                                    <div className="nav-item" >
                                        <Link to="#" className="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
                                            {String.allUAECities}
                                        </Link>
                                        <div className="dropdown-menu">
                                            {this.props.UAEcities && this.props.UAEcities.map((city, index) =>
                                                <Link key={index} to="#" className="dropdown-item">
                                                    {this.props.appLanguage === "en" ? city.cityName : city.arabicName}
                                                </Link>
                                            )}
                                        </div>
                                    </div>
                                </li>

                                <div className="nav-item dropdown">
                                    <div className="nav-item">

                                        {this.props.isLogged ? <Link className="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" to="/account/myprofile">
                                            {this.props.currentUser.username}
                                        </Link> : <li className="nav-item">
                                                <Link className="nav-link" to="/auth">
                                                    {String.joinOrSing}
                                                </Link>
                                            </li>}

                                        {this.props.isLogged &&
                                            <div className="dropdown-menu" >
                                                <Link to="#" className="dropdown-item">
                                                    <span onClick={() => this.redirectToAccount("/account/myprofile")}>
                                                        {String.myProfile}
                                                    </span>
                                                </Link>
                                                <Link to="#" className="dropdown-item">
                                                    <span onClick={() => this.redirectToAccount("/account/myAds")}>
                                                        {String.myAdstab}
                                                    </span>
                                                </Link>
                                                
                                                <Link to="#" className="dropdown-item">
                                                    <span onClick={() => this.redirectToAccount("/account/myfavourites")}>
                                                        {String.myFav}
                                                    </span>
                                                </Link>
                                                <Link to="#" className="dropdown-item">
                                                    <span onClick={() => { this.props.logOut(); this.redirectToAccount("/") }}>
                                                        {String.logOut}
                                                    </span>
                                                </Link>
                                            </div>}

                                    </div>
                                </div>

                                <li className="nav-item dropdown">
                                    {this.props.categories && this.props.categories.map((element, index) =>
                                        <div key={index} className="nav-item" >

                                            {/* {element.hasChild ? */}
                                            <Link className="nav-link dropdown-toggle" to={`/category/${element.id}/subcategory/all`} id="navbardrop" data-toggle="dropdown">
                                                {this.props.appLanguage === "en" ? element.name : element.arabicName}
                                            </Link>
                                            {/* : <li className="nav-item">
                                                    <Link className="nav-link" to="/CategoryCard" >
                                                        {this.props.appLanguage === "en" ? element.name : element.arabicName}
                                                    </Link>
                                                </li>} */}

                                            {element.hasChild &&
                                                <div className="dropdown-menu">
                                                    {element.child.map((child, index2) =>
                                                        <Link key={index2} to={`/category/${element.id}/subcategory/${child.id}`} className="dropdown-item">
                                                            {this.props.appLanguage === "en" ? child.name : child.arabicName}
                                                        </Link>
                                                    )}
                                                </div>
                                            }
                                        </div>
                                    )}
                                </li>

                                <li className="nav-item mt-2">
                                    <Link to="/placeYourAds" className="btn">{String.placeAds}</Link>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>



                {/* end mobile header */}


                {this.state.redirect && <Redirect to={this.state.redirectTo} />}

                <div className="header-large-screen">

                    <div className="first-header d-flex align-items-center " style={{ height: "32px" }} >
                        <div className="container d-flex justify-content-end">
                            <div className="row d-flex ">
                                <Link to="#" style={{ fontSize: "12px" }} >
                                    <span onClick={() => this.props.changeLanguage()} > {String.language} </span>
                                </Link>
                                <span className="mx-2 first-header-span-border " ></span>
                                <Link to="#" style={{ fontSize: "12px" }}>
                                    <span> {String.help} </span>
                                </Link>
                            </div>
                        </div>
                    </div>

                    <div className="second-header">
                        <div className="container">
                            <div className="row d-flex justify-content-between" >

                                <Link to="/" >
                                    <img src={logo} alt="prop" className="d-inline-block align-top logo-image" />
                                </Link>

                                <ul className="nav nav-tabs" style={{ lineHeight: "40px" }} >
                                    <li className="nav-item ml-auto">
                                        <div className="header-min-border" >
                                            <div style={{ cursor: "pointer" }} 
                                                 className="nav-link third-dropdown-parent header-min-border opacity-navlink">
                                                {/* { 
                                                    this.props.UAEcities && this.getCurrentCity() 
                                                    this.props.currentCity === "all" ?
                                                    (this.props.appLanguage === "en" ? 
                                                        String.allCities+'('+"Country"+')'
                                                        :
                                                        String.allCities+'('+"البلد"+')')
                                                    :
                                                    this.props.currentCity     
                                                } */}
                                                 {String.allUAECities}

                                                <div className="drop-list-wow" style={{ top: "63px" }}>
                                                    <ul className="nav flex-column p-0" >
                                                        
                                                        <li className="nav-item drop-down-item d-flex " onClick={ () => this.setCurrentCity("all") } > 
                                                            {String.all} 
                                                        </li>
                                                        
                                                        {this.props.UAEcities && this.props.UAEcities.map((city, index) =>
                                                            <li key={index} className=" nav-item drop-down-item d-flex"
                                                                onClick={() => this.setCurrentCity(index)  }>
                                                                {this.props.appLanguage === "en" ? city.cityName : city.arabicName}
                                                            </li>
                                                        )}
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </li>

                                    <li className="nav-item">
                                        <div className="header-min-border" >
                                            {/* <Link className="nav-link" to={this.props.isLogged?"Chat":"/auth"} > {String.myChats} </Link> */}
                                            <Link className="nav-link" to={this.props.isLogged?"/newDesgin/all":"/auth"} > {String.myChats} </Link>
                                        </div>
                                    </li>

                                    {/* <span className="second-border-span" ></span> */}

                                    <li className="nav-item" style={{ position: "relative" }} >
                                        <div className="" >
                                            <Link className="nav-link third-dropdown-parent header-min-border opacity-navlink" to={this.props.isLogged ? "/account/myprofile" : "/auth"} >
                                                {this.props.isLogged ? this.props.currentUser.username : String.joinOrSing}

                                                {this.props.isLogged &&
                                                    <div className="drop-list-wow" style={{ top: "63px" }}>
                                                        <ul className="nav flex-column p-0" >
                                                            <li onClick={() => this.redirectToAccount("/account/myprofile")} className="nav-item drop-down-item">
                                                                <span className="d-flex"> {String.myProfile} </span>
                                                            </li>
                                                            <li onClick={() => this.redirectToAccount("/account/myAds")} className="nav-item drop-down-item">
                                                                <span className="d-flex"> {String.myAdstab} </span>
                                                            </li>
                                                           
                                                            <li onClick={() => this.redirectToAccount("/account/myfavourites")} className="nav-item drop-down-item">
                                                                <span className="d-flex"> {String.myFav} </span>
                                                            </li>
                                                            <li onClick={() => this.redirectToAccount("/account/updateprofile")} className="nav-item drop-down-item">
                                                                <span className="d-flex"> {String.accountSettings} </span>
                                                            </li>
                                                            <li onClick={() => { this.props.logOut(); this.redirectToAccount("/") }} className="nav-item drop-down-item">
                                                                <span className="d-flex"> {String.logOut} </span>
                                                            </li>
                                                        </ul>
                                                    </div>}

                                            </Link>
                                        </div>
                                    </li>

                                    {/* <span className="second-border-span" ></span> */}

                                    <li className="addBTN">
                                        <Link to="/placeYourAds" className="btn">{String.placeAds}</Link>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div className="third-header">
                        <div className="container">
                            <ul className="nav nav-tabs d-flex justify-content-between w-100 px-0">
                                {this.props.categories && this.props.categories.map((element, index) =>
                                    <li key={index} className="nav-item opacity-navlink third-dropdown-parent" >
                                        <Link className="nav-link  "
                                            to={`/category/${element.id}/subcategory/all`} >
                                            {this.props.appLanguage === "en" ? element.name : element.arabicName}
                                        </Link>

                                        {element.hasChild &&
                                            <div className={this.props.appLanguage === "en" ? "drop-list-wow enleft" : "drop-list-wow arright"}>
                                                <ul className="nav flex-column p-0 font-weight-normal" >
                                                    {element.child.map((child, index2) =>
                                                        <li key={index2} className="nav-item d-flex">
                                                            <Link to={`/category/${element.id}/subcategory/${child.id}`} className="nav-link w-100 pb-2">
                                                                {this.props.appLanguage === "en" ? child.name : child.arabicName}
                                                            </Link>
                                                        </li>
                                                    )}
                                                </ul>
                                            </div>
                                        }
                                    </li>
                                )}
                            </ul>
                        </div>
                    </div>

                </div>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        isLogged: state.userReducer.logged,
        currentUser: state.userReducer.user,
        UAEcities: state.headerReducer.UAEcities,
        categories: state.headerReducer.categories,
        currentCity: state.headerReducer.currentCity ,
        countries: state.headerReducer.countries
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {
        changeLanguage,
        logOut,
        fetchCities,
        fetchCategories,
        fetchCountries,
        setFilterCity
    },
    dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
