import React, { Component } from 'react';
import './chat.css';
import { endPoint, socketEndPoint } from '../../assets/apiEndPoint';
import axios from 'axios';
import { connect } from 'react-redux';
import String from '../../assets/locals';
import person from '../../assets/images/person.jpg';
import LastContact from './lastContacts';
import io from "socket.io-client";
import $ from 'jquery';

class NewDesgin extends Component {

    state = {
        responseMessages: [],
        contactList: [],
        textMsg: ""
    }

    socket = io(socketEndPoint, { query: { id: this.props.currentUser ? this.props.currentUser.id : null } });

    getAllMessages = (userId) => {
        let uri = `${endPoint}messages?userId=${this.props.currentUser.id}&friendId=${userId}`;
        axios.get(uri)
            .then(response => {
                this.setState({ responseMessages: response.data.data.reverse() });
            })
            .catch(err => console.log("error", err));
    }

    getContactList = () => {
        let uri = `${endPoint}messages/lastContacts?id=${this.props.currentUser.id}`;
        axios.get(uri)
            .then(response => this.setState({ contactList: response.data.data }))
            .catch(err => console.log("error", err));
    }

    componentDidMount() {

        if (this.props.match.params.id !== "all" && this.props.isLogged)
            this.getAllMessages(this.props.match.params.id);

        if (this.props.isLogged)
            this.getContactList();

        // this.socket.on("connect", () => { console.log("successfully") });

        this.socket.on("newMessage", (data) => {
            this.setState({ responseMessages: [...this.state.responseMessages, data] });
        });

        this.socket.on('done', (data) => {
            // this.setState({ responseMessages: [...this.state.responseMessages , data] });
        });

    }

    sendMessage = () => {
        if (this.state.textMsg.trim() !== "") {
            this.socket.emit("newMessage", {
                toId: this.props.match.params.id,
                data: {
                    text: this.state.textMsg,
                    image: "",
                    user: {
                        _id: this.props.currentUser.id
                    }
                }
            });

            $(".current-chat-content").animate({
                scrollTop:
                    $(".current-chat-content")[0].scrollHeight
            }, 1000);


            let message = {
                text: this.state.textMsg,
                user: {
                    _id: this.props.currentUser.id,
                    img: this.props.currentUser.img
                }
            }
            this.setState({ textMsg: "", responseMessages: [...this.state.responseMessages, message] });
        }
    }

    componentDidUpdate(prevProp) {
        if (prevProp.match.params.id !== this.props.match.params.id)
            if (this.props.isLogged) this.getAllMessages(this.props.match.params.id);
    }

    render() {

        String.setLanguage(this.props.appLanguage);

        return (
            <div className="n-chat-wrapper px-3" >

                {!this.props.isLogged &&
                    <div className="container m-5 p-5 text-center" >
                        <h5> <strong> {String.noChatsYet} </strong> </h5>
                    </div>
                }

                {this.props.isLogged &&
                    <div className="row" >

                        <LastContact contactList={this.state.contactList} currentChat={this.props.match.params.id} />

                        <div className="col-sm-12 col-lg-9 current-cnotact-wrapper p-0" >
                            <div className="current-chat-content pt-2" >

                                {this.state.responseMessages.map((item, index) => {
                                    return parseInt(item.user._id) === parseInt(this.props.currentUser.id) ?

                                        <div key={index} className="my-messages d-flex justify-content-end align-items-center align-content-center mb-1 " >
                                            <p style={{ backgroundColor: '#679C8A' , paddingLeft: '10px',paddingRight: '10px',padding:'4px' }} className="my-message-body" >
                                                {item.text}
                                            </p>
                                            <img src={item.user.img ? item.user.img : person} alt="me" />
                                        </div>

                                        :

                                        <div key={index} className="my-friend-messages d-flex justify-content-start align-items-center align-content-center mb-1 " >
                                            <img src={item.user.img ? item.user.img : person} alt="me" />
                                            <p className="my-message-body" style={{ padding : '4px' }}  > {item.text} </p>
                                        </div>
                                }
                                )}

                            </div>

                            <div className="send-message-wrapper d-flex justify-content-center align-items-center " >

                                {/* <button className="mx-3 btn "  >
                                    <i className="fa fa-paperclip send-paperclip  " aria-hidden="true"></i>
                                </button> */}

                                <input type="text" className="form-control" value={this.state.textMsg}
                                    onChange={(e) => { this.setState({ textMsg: e.target.value }) }} />

                                <button className="mx-3 btn " style={{backgroundColor:"#679C8A",color:'#fff'}} onClick={this.sendMessage} > SEND </button>
                            </div>
                        </div>

                    </div>
                }
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        isLogged: state.userReducer.logged,
        currentUser: state.userReducer.user
    }
}
export default connect(mapStateToProps, null)(NewDesgin);