import React, { Component } from 'react';
import './chat.css';
import { connect } from 'react-redux';
import person from '../../assets/images/person.jpg';
import {socket} from './socket' ; 

class CurrentContact extends Component {

    state = {
        textMsg: ""
    }

    getDate = (date) => {
        let fullDate = new Date(date) ;
        let hours = fullDate.getHours() ; 
        let minutes = fullDate.getMinutes();

        if( hours > 11 ) return `${hours-12}:${minutes} PM` ; 
        else return `${hours}:${minutes} AM` ;
    }

    componentDidMount() {
        socket.on("connect" , () => console.log("connected succefully")  ) ; 
    }

    render() {

        return (
            <div className="col-sm-12 col-lg-9 current-cnotact-wrapper" >
                <div className="current-chat-content pt-2" >

                    {/* {arr.map(item => */}
                    {this.props.responseMessages.map((item, index) => {
                        return parseInt(item.user._id) === parseInt(this.props.currentUser.id) ?
                            // <div key={item} >
                            <div key={index} className="my-messages d-flex justify-content-end align-content-center mb-1 " >
                                <p style={{ backgroundColor: '#679C8A'}} className="my-message-body" >
                                    {item.text}
                                </p>
                                <img src={item.user.img ? item.user.img : person} alt="me" />
                            </div>

                            :

                            <div key={index} className="my-friend-messages d-flex justify-content-start align-content-center mb-1 " >
                                <img src={item.user.img ? item.user.img : person} alt="me" />
                                <p className="my-message-body"  > {item.text} </p>
                            </div>
                    }
                        // </div>
                    )}

                </div>

                <div className="send-message-wrapper d-flex justify-content-center align-items-center " >

                    <button className="mx-3 btn "  >
                        <i className="fa fa-paperclip send-paperclip  " aria-hidden="true"></i>
                    </button>

                    <input type="text" className="form-control" value={this.state.textMsg}
                        onChange={(e) => { this.setState({ textMsg: e.target.value }) }} />

                    <button className="mx-3 btn " onClick={this.socketStart } > SEND </button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        isLogged: state.userReducer.logged,
        currentUser: state.userReducer.user,
        UAEcities: state.headerReducer.UAEcities,
        categories: state.headerReducer.categories
    }
}
export default connect(mapStateToProps, null)(CurrentContact);