import React, { Component } from 'react';
import './chat.css';
import person from '../../assets/images/person.jpg';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class LastContact extends Component {

    getDate = (date) => {
        let fullDate = new Date(date);
        let hours = fullDate.getHours();
        let minutes = fullDate.getMinutes();

        if (hours > 11) return `${hours - 12}:${minutes} PM`;
        else return `${hours}:${minutes} AM`;
    }

    render() {

        return (
            <div className="col-sm-12 col-lg-3 last-contact-wrapper px-0 " >

                {this.props.contactList.map((item, index) => {

                    return parseInt(item.to.id) !== parseInt(this.props.currentUser.id) ?
                        <Link key={index} className="contact-item" to={`/newDesgin/${item.to.id}`} style={{ color: "unset", textDecoration: "unset" }} >
                            <div className="p-4 d-flex"
                                style={{ backgroundColor: (parseInt(item.to.id) === parseInt(this.props.currentChat) ? "#679C8A" : "white")
                                ,height: '95px',color: (parseInt(item.to.id) === parseInt(this.props.currentChat) ? "#fff" : "#000") }} >
                                <img src={item.to.img ? item.to.img : person} className="n-chat-image" alt="user-face" />

                                <div className="d-flex flex-column w-100 ml-3" >
                                    <div className="d-flex justify-content-between " >
                                        <h6> {item.to.username} </h6>
                                        <span className="contact-font-size" > {this.getDate(item.incommingDate)} </span>
                                    </div>
                                    <p className="m-0 contact-font-size text-truncate" style={{ maxWidth: "160px" }} > {item.content} </p>
                                </div>
                            </div>
                        </Link>

                        :

                        <Link key={index} className="contact-item" to={`/newDesgin/${item.from.id}`} style={{ color: "unset", textDecoration: "unset" }} >
                            <div className="p-4 d-flex"
                                style={{ backgroundColor: (parseInt(item.from.id) === parseInt(this.props.currentChat) ? "#679C8A" : "white") }} >
                                <img src={item.from.img ? item.from.img : person} className="n-chat-image" alt="user-face" />

                                <div className="d-flex flex-column w-100 ml-3" >
                                    <div className="d-flex justify-content-between " >
                                        <h5> {item.from.username} </h5>
                                        <span className="contact-font-size" > {this.getDate(item.incommingDate)} </span>
                                    </div>
                                    <p className="m-0 contact-font-size text-truncate" style={{ maxWidth: "160px" }} > {item.content} </p>
                                </div>
                            </div>
                        </Link>
                }

                )}
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        currentUser: state.userReducer.user
    }
}
export default connect(mapStateToProps, null)(LastContact);