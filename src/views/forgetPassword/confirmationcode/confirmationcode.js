import React, { Component } from 'react';
import times from '../../../assets/images/times.png';
import logo from '../../../assets/images/Logogreen.png';
import { Link , Redirect } from 'react-router-dom';
import './confirmationcode.css'
import String from '../../../assets/locals';
import { connect } from 'react-redux';
import { endPoint } from '../../../assets/apiEndPoint';
import axios from 'axios';

class Confirmationcode extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
            redirectToResetPassword: false
        };
    }

    confirmCodeApi = () => {
        let uri = `${endPoint}confirm-code`;
        axios.post(uri, {
            email: this.props.location.state.email,
            verifycode: this.state.fields["confirmcode"].trim() 

        }, {
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => { this.setState({ redirectToResetPassword : true }) ; })
            .catch(err => console.log("err", err));
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["confirmcode"] || fields["confirmcode"].trim() === "") {
            formIsValid = false;
            errors["confirmcode"] = String.pleaseEnterCode;
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.handleValidation()) {
            this.confirmCodeApi() ;
            // alert("Form submitted");
        } else {
            // alert("Form has errors.")
        }
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }

    render() {

        String.setLanguage(this.props.appLanguage);
        console.log("props location" , this.props.location ) ;
        return (
            <div className="confirmcode">

                {this.state.redirectToResetPassword &&
                 <Redirect to={{ pathname : "/resetpassword" , state : { email : this.props.location.state.email } }} /> }

                <div className="confirmcode-header text-center py-4">
                    <div>
                        <Link to="/" >
                            <img width="100" src={logo} alt="logo" />
                        </Link>
                    </div>
                </div>
                <div className="confirmcode-form">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-sm-12 mx-auto mt-5">
                                <div className="form-inputs">
                                    <div className="close-form mb-5">
                                        <Link to="/" >
                                            <img src={times} alt="close" />
                                        </Link>
                                    </div>
                                    
                                    <p className="font-weight-bold text-center" style={{fontSize : "1.2rem"}}> 
                                        {String.confirmText} 
                                    </p>
                                    
                                    <form onSubmit={this.onSubmit.bind(this)}>
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder={String.code} onChange={this.handleChange.bind(this, "confirmcode")} value={this.state.fields["confirmcode"]} />
                                            <p>
                                                <span style={{ color: "red" }}>{this.state.errors["confirmcode"]}</span>
                                            </p>
                                        </div>

                                        <div className="text-center submitBTN">
                                            <button type="submit" className="btn btn-block">{String.enterCode}</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage
    }
}
export default connect(mapStateToProps, null)(Confirmationcode);
