import React, { Component } from 'react';
import './email.css';
import times from '../../../assets/images/times.png'
import String from '../../../assets/locals';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import logo from '../../../assets/images/Logogreen.png'
import { Link , Redirect } from 'react-router-dom';
import {endPoint} from '../../../assets/apiEndPoint';
import axios from 'axios';
class Email extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {} , 
            redirectToConfirmCode : false 
        };
    }

    forgetApi = () => {

        let uri = `${endPoint}sendCode`;
        axios.post(uri, { email: this.state.fields["email"].trim() }, {
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => { this.setState({redirectToConfirmCode:true}) ;})
            .catch(err => { console.log("error") })
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["email"] || fields["email"].trim() === "") {
            formIsValid = false;
            errors["email"] = "Please Enter Your Email";
        }

        else if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.handleValidation()) {
            this.forgetApi() ; 
            // alert("Form submitted");
        } else {
            // alert("Form has errors.")
        }

    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }

    render() {
        String.setLanguage(this.props.appLanguage);
        return (
            <div className="forgetPass">

                {this.state.redirectToConfirmCode && 
                    <Redirect to={{ pathname : "/Confirmationcode" , state : { email : this.state.fields["email"] } }} /> }

                <div style={{ borderBottom: "1px solid #dee2e6" }} className="text-center py-3">
                    <div><Link to="/" ><img width="90" src={logo} alt="logo" /></Link></div>
                </div>
                <div className="forgetPass-form">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-sm-12 mx-auto mt-5">
                                <div className="form-inputs">
                                    <div className="close-form">
                                        <img src={times} alt="close" />
                                    </div>
                                    <h4 style={{ width: "70%", margin: "auto" }} className="text-center pb-2 pt-3">{String.forgetPassword}</h4>
                                    <p style={{ width: "70%", margin: "auto", fontSize: "14px" }} className="text-center pt-3 pb-3">{String.enterEmail}</p>

                                    <form onSubmit={this.onSubmit.bind(this)}>

                                        <div className="form-group">
                                            <input type="email" className="form-control" placeholder={String.EmailForget} onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} />
                                            <p>
                                                <span style={{ color: "red" }}>{this.state.errors["email"]}</span>
                                            </p>
                                        </div>

                                        <div className="text-center submitBTN">
                                            <button type="submit" className="btn btn-block">{String.resetPassword}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {

    },
    dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(Email); 