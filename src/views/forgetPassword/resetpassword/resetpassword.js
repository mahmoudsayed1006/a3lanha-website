import React, { Component } from 'react';
import './resetpassword.css';
import times from '../../../assets/images/times.png';
import logo from '../../../assets/images/Logogreen.png';
import String from "../../../assets/locals";
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { endPoint } from '../../../assets/apiEndPoint';
import axios from 'axios';

class ResetPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
            redirectToLogin: false
        };
    }

    resetPasswordApi = () => {

        let uri = `${endPoint}reset-password`;

        axios.post(uri, {
            newPassword: this.state.fields['password'],
            email: this.props.location.state.email
        }, {
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => this.setState({ redirectToLogin: true }))
            .catch(err => console.log("err" , err));
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = String.passwordRequired;
        }
        else if (typeof fields["password"] !== "undefined") {
            if (fields["password"].length < 7) {
                formIsValid = false;
                errors["password"] = String.weakPassword;
            }
        }

        if (!fields["condirmpassword"]) {
            formIsValid = false;
            errors["condirmpassword"] = String.confirmPassword;
        }

        if (fields["condirmpassword"] !== fields['password']) {
            formIsValid = false;
            errors["condirmpassword"] = String.passwordDoesntMatch;
        }


        this.setState({ errors: errors });
        return formIsValid;
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.handleValidation()) {
            this.resetPasswordApi();
            // alert("Form submitted");
        } else {
            // alert("Form has errors.")
        }

    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }

    render() {

        String.setLanguage(this.props.appLanguage);

        return (
            <div className="forgetPass">

                {this.state.redirectToLogin &&
                    <Redirect to='/auth/login' />}

                <div className="forgetPass-header text-center py-4">
                    <div>
                        <Link to="/" >
                            <img width="100" src={logo} alt="logo" />
                        </Link>
                    </div>
                </div>
                <div className="forgetPass-form">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-sm-12 mx-auto mt-5">
                                <div className="form-inputs">
                                    <div className="close-form mb-5">
                                        <Link to="/" >
                                            <img src={times} alt="close" />
                                        </Link>
                                    </div>

                                    <p className="font-weight-bold text-center" style={{fontSize : "1.2rem"}}> 
                                        {String.resetPasswordText} 
                                    </p>

                                    <form onSubmit={this.onSubmit.bind(this)}>
                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder={String.password} onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} />
                                            <p style={{ textAlign: this.props.appLanguage === "en" ? "left" : "right" }} >
                                                <span style={{ color: "red" }}>{this.state.errors["password"]}</span>
                                            </p>
                                        </div>

                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder={String.confirmYourPassword} onChange={this.handleChange.bind(this, "condirmpassword")} value={this.state.fields["condirmpassword"]} />
                                            <p className="d-flex" >
                                                <span style={{ color: "red" }}>{this.state.errors["condirmpassword"]}</span>
                                            </p>
                                        </div>

                                        <div className="text-center submitBTN">
                                            <button type="submit" className="btn btn-block">{String.resetPassword}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage
    }
}
export default connect(mapStateToProps, null)(ResetPassword);
