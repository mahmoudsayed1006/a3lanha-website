import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import String from '../../assets/locals';
import './ContactUs.css'
import axios from 'axios';
import { endPoint } from '../../assets/apiEndPoint';

class ContactUs extends Component {

    state = {
        message: "",
        toHome: false,
        messageError: "",
        number:"",
        email:"",
        name:"",
    }

    contactUsApi = () => {

        let uri = `${endPoint}contact-us`;
        axios.post(uri, {
            name: this.state.name,
            number: this.state.phone,
            email: this.state.email,
            message: this.state.message
        }, {
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                alert(String.yourMessageSent);
                this.setState({ toHome: true })
            })
            .catch(err => alert(String.someThingWrong))
    }

    validate = () => {
        if (!this.state.message || this.state.message.trim() === "") {
            this.setState({ messageError: String.messageRequired });
        }
        else {
            this.contactUsApi();
        }
    }

    render() {
        String.setLanguage(this.props.appLanguage);
        return (
            <div className="ContactUs">

                {!this.props.isLogged && <Redirect to="/" />}
                {this.state.toHome && <Redirect to="/" />}

                <div className="ContactUs-header py-3 text-center">
                    <h3>{String.contactUsTitle}</h3>
                </div>
                <div className="ContactUs-content mb-5">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-8 mx-auto">
                                <div className="form-group" >
                                <input style={{marginBottom: '10px'}} placeholder={String.userName} type="text" className="form-control" name="title"
                                onChange={(e) => this.setState({ name: e.target.value })} value={this.state.name}/>
                                <input style={{marginBottom: '10px'}} placeholder={String.email} type="email" className="form-control" name="title"
                                onChange={(e) => this.setState({ email: e.target.value })} value={this.state.email}/>
                                <input style={{marginBottom: '10px'}} placeholder={String.phone} type="text" className="form-control" name="title"
                                onChange={(e) => this.setState({ phone: e.target.value })} value={this.state.phone}/>
                                    <textarea rows="5" className="form-control" value={this.state.message}
                                        onChange={(e) => this.setState({ message: e.target.value })} />

                                    {this.state.messageError !== "" &&
                                        <p className="d-flex" >
                                            <span style={{ color: "red" }} > {this.state.messageError} </span>
                                        </p>
                                    }
                                </div>
                                <button onClick={this.validate} className="btn btn-block py-2" style={{ backgroundColor : "#679C8A",color:'#fff' }} >{String.submit}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        isLogged: state.userReducer.logged,
        currentUser: state.userReducer.user,
        appLanguage: state.headerReducer.appLanguage

    }
}

export default connect(mapStateToProps, null)(ContactUs); 
