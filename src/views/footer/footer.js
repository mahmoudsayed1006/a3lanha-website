import React, { Component } from 'react';
import './footer.css'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeLanguageWithParams , fetchCities } from "../../redux/Actions/headerActions" ;
import String from "../../assets/locals" ; 
import {currentCountryFun,currentCityFun} from '../../redux/Actions/userActions'

class Footer extends Component {

    componentDidMount(){
        console.log("lang   ",this.props.appLanguage)
    }

    render() {

        String.setLanguage(this.props.appLanguage) ; 

        return (
            <div className="a3lanha-footer" id="home-body-opacity">
                <div className="container" >
                    <div className="row  py-5" >

                        <div className="col-sm-12 col-md-4 col-lg-2 mb-3">
                            <span className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}  > {String.comapany} </span>
                            <ul className="nav flex-column p-0">
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="/AboutUs">{String.aboutUs}</Link>
                                </li>
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}} >
                                    <Link className="nav-link" to="TermsofUse">{String.termsOfUs}</Link>
                                </li>
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="/Usage">{String.usage}</Link>
                                </li>
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="/PrivacyPolicy">{String.privacyPoilcy}</Link>
                                </li>
                            </ul>
                        </div>

                        <div className="col-sm-12 col-md-4 col-lg-2 mb-3" >
                            <span className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}} > {String.cities} </span>
                            <ul className="nav flex-column p-0">
                                {this.props.UAEcities && this.props.UAEcities.map((city ,index) => {
                                    return (
                                        <li key={index} className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                            <Link onClick={()=>{
                                                this.props.currentCityFun(city)
                                            }} className="nav-link" to="#">
                                                { this.props.appLanguage === "en" ? city.cityName : city.arabicName }
                                            </Link>
                                        </li>
                                    )
                                })}
                            </ul>
                        </div>

                    
                        <div className="col-sm-12 col-md-4 col-lg-2 mb-3">
                            <span className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}> {String.otherCountries} </span>
                            <ul className="nav flex-column p-0">
                                {this.props.countries.map((country , index) => 
                                        <li key={index} className={"nav-item " +  (this.props.appLanguage==="ar" ? "text-right" : "")  } style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}} >
                                            <Link className="nav-link" to="#" onClick={()=>{
                                                this.props.fetchCities(country.id)
                                                this.props.currentCountryFun(country) 
                                                }} >
                                                {this.props.appLanguage === "en" ? country.countryName : country.arabicName}
                                            </Link>
                                        </li>
                                )}
                            </ul>
                        </div>

                        <div className="col-sm-12 col-md-4 col-lg-2 mb-3">
                            <span className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}> {String.social} </span>
                            <ul className="nav flex-column p-0">
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="#">{String.socialFacebook}</Link>
                                </li>
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="#">{String.socialTwitter}</Link>
                                </li>
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="#">{String.Youtube}</Link>
                                </li>
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="#">{String.Instagram}</Link>
                                </li>
                            </ul>
                        </div>

                        <div className="col-sm-12 col-md-4 col-lg-2 mb-3">
                            <span className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}} > {String.support} </span>
                            <ul className="nav flex-column p-0">
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="#">{String.supportHelp}</Link>
                                </li>
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" to="/ContactUs">{String.contactUs}</Link>
                                </li>
                            </ul>
                        </div>

                        <div className="col-sm-12 col-md-4 col-lg-2 mb-3">
                            <span className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}> {String.languages} </span>
                            <ul className="nav flex-column p-0">
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" onClick={()=> this.props.changeLanguageWithParams("en")} to="#">English</Link>
                                </li>
                                <li className="nav-item d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'}}>
                                    <Link className="nav-link" onClick={()=> this.props.changeLanguageWithParams("ar")} to="#">العربية</Link>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage ,
        UAEcities: state.headerReducer.UAEcities ,
        countries : state.headerReducer.countries
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {
        changeLanguageWithParams ,
        fetchCities,
        currentCityFun,
        currentCountryFun
    },
    dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Footer);

// export default Footer; 