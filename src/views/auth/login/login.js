import React, { Component } from 'react';
import './login.css';
import logo from '../../../assets/images/Logogreen.png';
import times from '../../../assets/images/times.png';
import { Link, Redirect } from 'react-router-dom';
import String from "../../../assets/locals";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { saveUser,currentCountryFun } from "../../../redux/Actions/userActions";
import { endPoint, localStorageUserToken } from '../../../assets/apiEndPoint';
import axios from "axios";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
            redirectToHome: false,
            activeButton: false,
            addAdsLoad:false,

        };
    }
    MyVerticallyCenteredModal = () => {
        return (
            <div style={{zIndex:2000, backgroundColor:'rgba(0,0,0,0.5)', position:'fixed',width:'100%',height:'100%', top:0, display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}} >
            <Loader
            visible={true}
            type="Triangle"
            color="#679C8A"
            height={150}
            width={150}                    
            />
            <spa>Wait...</spa>
         </div>
        );
      }
    loginApi = () => {

        let uri = `${endPoint}signin`;
        this.setState({addAdsLoad:true})

        axios.post(uri, { email: this.state.fields.email, password: this.state.fields.password }, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            this.props.saveUser(res.data.token, res.data.user);
           
            this.setState({addAdsLoad:false, redirectToHome: true })
        })

            .catch(err => {
                let errors = {};
                errors.loginError = String.loginErrorApi;
                this.setState({ errorsوaddAdsLoad:false }); 
            })
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = String.emailRequired;
        }

        else if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = String.invaildEmail;
            }
        }

        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = String.passwordRequired;
        }
        else if (typeof fields["password"] !== "undefined") {
            if (fields["password"].length < 2) {
                formIsValid = false;
                errors["password"] = String.weakPassword;
            }
        }

        this.setState({ errors: errors });
        return formIsValid;
    }


    onSubmit(e) {
        e.preventDefault();

        if (this.handleValidation()) {
            this.loginApi();
        } else {
            // alert("Form has errors.")
        }

    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;

        if (fields["email"] && fields["email"].trim() !== "" && fields["password"] && fields["password"].trim() !== "")
            this.setState({ fields, activeButton: true })
        else
            this.setState({ fields, activeButton: false });
    }

    render() {

        String.setLanguage(this.props.appLanguage);
        const loggedAlready = localStorage.getItem(localStorageUserToken);

        return (
            <div className="login">

                {this.state.redirectToHome && <Redirect to="/" />}
                {loggedAlready && <Redirect to="/" />}

                <div className="login-header text-center py-3">
                    <div>
                        <Link to="/" >
                            <img width="90" src={logo} alt="prop" className="d-inline-block align-top reg-logo-image" />
                        </Link>
                    </div>
                </div>

                <div className="login-form">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-sm-12 mx-auto" style={{ marginTop: "2rem" }}>
                                <div className="form-inputs">
                                    <div className="close-form" style={{ marginBottom: "32px" }}>
                                        <Link to="/auth">
                                            <img src={times} alt="close" />
                                        </Link>
                                    </div>
                                    <h1 style={{ fontSize: "24px", marginBottom: "32px" }} className="text-center">{String.logToWebsite}</h1>

                                    <form onSubmit={this.onSubmit.bind(this)}>

                                        <div className="form-group">
                                            <input type="email" className="form-control" placeholder="Email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} />
                                            <p className="d-flex" >
                                                <span style={{ color: "red" }}>{this.state.errors["email"]}</span>
                                            </p>
                                        </div>

                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder="Password" onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} />
                                            <p className="d-flex" >
                                                <span style={{ color: "red" }}>{this.state.errors["password"]}</span>
                                            </p>
                                        </div>

                                        <div className="pb-3 pt-2 text-center forgetBTN">
                                            <Link to={{ pathname: "/forgetPassword" }}>
                                                {String.forgetPassword}
                                            </Link>
                                        </div>

                                        <div className="text-center submitBTN">
                                            <button disabled={!this.state.activeButton} type="submit" className="btn btn-block">{String.loginButton}</button>
                                        </div>

                                        <p className="font-weight-bold mt-4" style={{ fontSize: "1.2rem" ,color:"red" }} >
                                            {this.state.errors.loginError}
                                        </p>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                {this.state.addAdsLoad&&
                this.MyVerticallyCenteredModal()
               }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        loggedUser: state.userReducer.user,
        currentCountry: state.userReducer.currentCountry
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {
        saveUser,
        currentCountryFun,
    },
    dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Login);