import React, { Component } from 'react';
import './register.css';
import times from '../../../assets/images/times.png'
import logo from '../../../assets/images/Logogreen.png'
import { Link, Redirect } from "react-router-dom";
import String from '../../../assets/locals';
import { connect } from 'react-redux';
import { endPoint } from '../../../assets/apiEndPoint';
import { saveUser,currentCountryFun } from "../../../redux/Actions/userActions";
import { bindActionCreators } from "redux";
import axios from 'axios';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: { country : -1 },
            errors: {},
            selectCountry: -1,
            addAdsLoad:false,
            redirectToHome: false ,
            activeButton : false 
        };
    }

    signUpApi() {

        let { fields } = this.state;
        let data = new FormData();

        data.append("username", fields.name);
        data.append("email", fields.email);
        data.append("phone", fields.phone);
        data.append("country", this.props.countries[ parseInt(this.state.fields.country)].countryName);
        data.append("countryId", this.props.countries[ parseInt(this.state.fields.country)].id  );
        data.append("type", "CLIENT");
        data.append("password", fields.password);

        let uri = `${endPoint}signup`;
        this.setState({addAdsLoad:true})
        axios.post(uri, data).then(res => {
            //this.props.currentCountryFun(this.props.countries[fields.country])
            console.log("User   ",res.data)
            this.props.saveUser(res.data.token, res.data.user);

        })
        .then(res => 
         this.setState({addAdsLoad:false, redirectToHome: true }))
        .catch(err => { alert(String.someThingWrong) ; this.setState({addAdsLoad:false})});

    } 
    MyVerticallyCenteredModal = () => {
        return (
            <div style={{zIndex:2000, backgroundColor:'rgba(0,0,0,0.5)', position:'fixed',width:'100%',height:'100%', top:0, display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}} >
            <Loader
            visible={true}
            type="Triangle"
            color="#679C8A"
            height={150}
            width={150}                    
            />
            <spa>Wait...</spa>
         </div>
        );
      }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["name"]) {
            formIsValid = false;
            errors["name"] = String.nameRequired;
        }
        else if (typeof fields["name"] !== "undefined") {
            if (!fields["name"].match(/^[a-zA-Z\s]+$/)) {
                formIsValid = false;
                errors["name"] = "Name must be Only letters";
            }
        }

        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = String.emailRequired;
        }

        else if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                // errors["email"] = "Email is not valid";
                errors["email"] = String.invaildEmail;
            }
        }

        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = String.passwordRequired;
        }
        else if (typeof fields["password"] !== "undefined") {
            if (fields["password"].length < 7) {
                formIsValid = false;
                errors["password"] = String.weakPassword;
            }
        }

        if (!fields["phone"]) {
            formIsValid = false;
            errors["phone"] = String.phoneRequired
        }
        else if (typeof fields["phone"] !== "undefined") {
            if (!fields["phone"].match(/^[0-9]+$/)) {
                formIsValid = false;
                // errors["phone"] = "Phone must be Numbers only";
                errors["phone"] = String.phoneNumbersOnly;
            }
        }

        if ( parseInt(fields["country"]) === -1) {
            formIsValid = false;
            errors["selectedCountry"] = String.countryRequired;
        }


        this.setState({ errors: errors });
        return formIsValid;
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.handleValidation()) {
            // alert("Form submitted");
            this.signUpApi();
        } else {
            // alert("Form has errors.")
        }

    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        console.log("country test ",e.target.value)

        if (fields["email"] && fields["email"].trim() !== "" && fields["name"] && fields["name"].trim() !== "" &&
            fields["password"] && fields["password"].trim() !== "" && fields["phone"] && fields["phone"].trim() !== ""
            && fields["country"] && fields["country"] !== -1 )
            this.setState({ fields, activeButton: true });

        else this.setState({ fields, activeButton: false });

    }

    render() {

        String.setLanguage(this.props.appLanguage);

        return (
            <div className="registration">

                {this.state.redirectToHome && <Redirect to="/" />}

                <div className="registration-header text-center py-3">
                    <div>
                        <Link to="/" >
                            <img width="90" src={logo} alt="prop" className="d-inline-block align-top reg-logo-image" />
                        </Link>
                    </div>
                </div>

                <div className="registration-form">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-sm-12 mx-auto" style={{ marginTop: "2rem" }}>
                                <div className="form-inputs">
                                    {/* <div><i style={{cursor: "pointer"}} className="fa fa-times fa-2x"></i></div> */}
                                    <div className="close-form" style={{ marginBottom: "32px" }}>
                                        <Link to="/auth">
                                            <img src={times} alt="close" />
                                        </Link>
                                    </div>
                                    <h1 style={{ fontSize: "24px", marginBottom: "32px" }} className="text-center"> {String.signUp + String.siteName}</h1>

                                    <form onSubmit={this.onSubmit.bind(this)}>
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder={String.firstName} onChange={this.handleChange.bind(this, "name")} value={this.state.fields["name"]} />
                                            <p className="d-flex" >
                                                <span style={{ color: "red" }}>{this.state.errors["name"]}</span>
                                            </p>
                                        </div>

                                        <div className="form-group">
                                            <input type="email" className="form-control" placeholder={String.userEmail} onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} />
                                            <p className="d-flex" >
                                                <span style={{ color: "red" }}>{this.state.errors["email"]}</span>
                                            </p>
                                        </div>

                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder={String.password} onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} />
                                            <p className="d-flex" >
                                                <span style={{ color: "red" }}>{this.state.errors["password"]}</span>
                                            </p>
                                        </div>

                                        <div className="form-group" >
                                            <select className="form-control" value={this.state.fields["country"]}
                                                // onChange={(e) => this.setState({ selectCountry: e.target.value })}
                                                onChange={
                                                     this.handleChange.bind(this, "country") 
                                                     
                                                    }
                                                
                                                 >
                                                <option value={-1} selected disabled> {String.selectCountry} </option>
                                                {this.props.countries.map((item, index) => 
                                                    <option value={index} key={index} > 
                                                        { this.props.appLanguage === "en" ? item.countryName : item.arabicName} 
                                                    </option>)}
                                            </select>
                                            <p className="d-flex" >
                                                <span style={{ color: "red" }}>{this.state.errors["selectedCountry"]}</span>
                                            </p>
                                        </div>

                                        <div className="form-group" >
                                            <input type="text" className="form-control" placeholder={String.userPhone} onChange={this.handleChange.bind(this, "phone")} value={this.state.fields["phone"]} />
                                            <p className="d-flex" >
                                                <span style={{ color: "red" }}>{this.state.errors["phone"]}</span>
                                            </p>
                                        </div>

                                        <div className="text-center submitBTN">
                                            <button disabled={!this.state.activeButton} type="submit" className="btn btn-block">{String.signUpButton}</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.addAdsLoad&&
                this.MyVerticallyCenteredModal()
               }
            </div >
        );
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage ,
        countries: state.headerReducer.countries,
        currentCountry: state.userReducer.currentCountry
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {
        saveUser ,
        currentCountryFun
    },
    dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(Register); 