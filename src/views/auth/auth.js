import React, { Component } from 'react';
import "./register/register.css";
import logo from "../../assets/images/Logogreen.png";
import times from '../../assets/images/times.png'
import { Link } from 'react-router-dom';
import String from "../../assets/locals";
import { connect } from 'react-redux';
import { GoogleLogin } from 'react-google-login';
import "./auth.css";
import FacebookLogin from 'react-facebook-login';

const responseGoogle = (response) => {
    console.log(response);
  }
const responseFacebook = (response) => {
    console.log(response);
  }
class Auth extends Component {

    render() {
        String.setLanguage(this.props.appLanguage);
        return (
            <div className="registration">

                <div className="registration-header text-center py-4">
                    <div>
                        <Link to="/" >
                            <img src={logo} alt="prop" className="d-inline-block align-top reg-logo-image" />
                        </Link>
                    </div>
                </div>

                <div className="registration-form">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-sm-12 mx-auto mt-5">
                                <div className="form-inputs">
                                    <div className="close-form">
                                        <Link to="/">
                                            <img src={times} alt="close" />
                                        </Link>
                                    </div>
                                    <h5 className="text-center py-4"> {String.logOrSignUp}</h5>
                                    <div className="row" >
                                        <div className="text-center col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-3" >
                                            <Link  style={{color:'#fff'}} to="/auth/register" className="btn btn-block auth-color-buttons">{String.signUpButton}</Link>
                                        </div>
                                        <div className="text-center col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-3">
                                            <Link style={{color:'#fff'}} to="/auth/login" className="btn btn-block auth-color-buttons">{String.loginButton}</Link>
                                        </div>
                                    </div>

                                    <div className="row justify-content-center " >
                                        <p style={{fontWeight:"700"}} className="text-center m-3" > 
                                            {String.agreeText} <Link to="/TermsofUse" style={{color:"#679C8A"}}> {String.authTerms} </Link > {String.authAnd} <Link to="/PrivacyPolicy" style={{color:"#679C8A"}}> {String.authPrivacy} </Link> 
                                        </p>
                                    </div>
                                    <div className="social">
                                    <GoogleLogin style={{width:"100%"}}
                                        clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
                                        
                                        buttonText="     Login With Google      "
                                        onSuccess={responseGoogle}
                                        onFailure={responseGoogle}
                                        cookiePolicy={'single_host_origin'}
                                    />
                                   <FacebookLogin
                                        appId="1088597931155576"
                                        autoLoad={true}
                                        fields="name,email,picture"
                                        callback={responseFacebook}
                                        icon="fa-facebook"
                                    />
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage
    }
}
export default connect(mapStateToProps, null)(Auth);
