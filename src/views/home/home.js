import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import String from '../../assets/locals';
import { connect } from 'react-redux';
import SearchComponent from './searchComponent';
import axios from 'axios';
import { endPoint } from '../../assets/apiEndPoint';

import "./home.css";
import Skeleton from 'react-loading-skeleton';
import { bindActionCreators } from "redux";
import {currentCountryFun} from '../../redux/Actions/userActions'

class Home extends Component {

    state = {
        trendCategories: [],
        ads:[],
        trendingLoad:true,
        loadArray:[1,2,3,4,5,6]
    }


    fetchTrending() {

        let uri = `${endPoint}categories/sub-categories?trend=true`;
        this.setState({trendingLoad:true})
        axios.get(uri)
            .then(res => {
                this.setState({trendingLoad:false, trendCategories: res.data.data });
            })
            .catch(err => {
                //this.setState({trendingLoad:false})
                console.log("err", err)
            });
    }
    fetchAds() {

        let uri = `${endPoint}anoncement`;
        axios.get(uri)
            .then(res => {
                this.setState({ ads: res.data.data });
            })
            .catch(err => console.log("err", err));
    }

    componentDidMount() {
        const currentCountry = localStorage.getItem('CURRENT_COUNTRY');
        if(currentCountry){
            console.log("exist ")
            this.props.currentCountryFun(JSON.parse(currentCountry))
        }else{
            console.log("not exist ")
        }
        this.fetchTrending();
        this.fetchAds();
    }



    render() {

        String.setLanguage(this.props.appLanguage);
      
        const ads = this.state.ads;
        console.log(ads)
        return (
            <div className="home-body" id="home-body-opacity">

                <SearchComponent />

                <div className="container" >
                    <div className="trendring" >
                        {/* <h3> {this.state.data.trending.title} on Website </h3> */}
                        <div className="d-flex" style={{ fontSize: "24px", margin: "48px 0 0",display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end' }} > <span >{String.trending} </span>   </div>
                        <div className="row">
                           
                            { this.state.trendingLoad?
                             this.state.loadArray.map(item=>(
                                <div className=" home-card col-sm-5 col-md-3 col-lg-2 ">
                                <Link className="nav-link p-0" style={{color:"unset"}}  >
                                    <img src={require('../../assets/images/loadImage.png')} alt="prop-img" />
                                </Link>
                                </div>
                             ))
                             :
                            this.state.trendCategories.map((item, index) =>
                                <div className=" home-card col-sm-5 col-md-3 col-lg-2 " key={index} >
                                    <Link className="nav-link p-0" style={{color:"unset"}} to={`/category/${item.parent}/subcategory/${item.id}`} >
                                        <img src={item.img} alt="prop-img" />
                                        <div className="d-flex" style={{display:'flex',justifyContent:'center'}} > {this.props.appLanguage === "en" ? item.name : item.arabicName} </div>
                                    </Link>
                                    {/* <span className="d-flex"  > {this.props.appLanguage === "ar" ? item.category.ar : item.category.en} </span> */}
                                </div>
                            )}

                           
                        </div>
                    </div>

                    <div className="popular mb-4">
                        <div className="d-flex" style={{ fontSize: "24px", margin: "48px 0 10px",display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'  }}>
                            <span >{String.popular} </span> </div>
                     {this.props.categoriesLoading?
                     <div className="row">
                         
                       {this.state.loadArray.map(item=>(
                        <div className=" home-card col-sm-5 col-md-3 col-lg-2 ">
                        <Link className="nav-link p-0" style={{color:"unset"}}  >
                            <img src={require('../../assets/images/loadImage.png')} alt="prop-img" />
                        </Link>
                        </div>
                     ))
                       }
                     </div>
                     :
                        <div className="row m-auto " >
                            {this.props.categories.map((item, index) =>
                                <div key={index} className="home-popular-subcats col-sm-12 col-md-6 col-lg-4 nopadding " >
                                    <h6 className="mt-4 d-flex px-1 " style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end' }} >
                                        <span style={{ fontWeight: "bold",}}>
                                            {this.props.appLanguage === "en" ? item.name : item.arabicName}
                                        </span>
                                    </h6>
                                    <ul className="nav px-0 " style={{ paddingLeft: 0,display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end'  }} >
                                        {item.hasChild && item.child.map((subcat, index2) =>
                                            <Link className="nav-link home-round-link m-1" key={index2}
                                                to={`/category/${item.id}/subcategory/${subcat.id}`} >
                                                {this.props.appLanguage === "en" ? subcat.name : subcat.arabicName}
                                            </Link>)}
                                    </ul>
                                </div>
                            )}
                        </div>
                    }
                    </div>
                </div>

                <div className="container home-ads mb-5" >
                    <div className="row" >
                    {ads.map(val=>(
                        <div className=" home-ads-img col-sm-12 col-md-6" >
                            <img src={val.img} alt="ad" />
                        </div>
                    ))} 
                        
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        categories: state.headerReducer.categories,
        categoriesLoading: state.headerReducer.categoriesLoading,
    }
}

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        currentCountryFun
    },
    dispatch
);

export default connect(mapStateToProps,mapDispatchToProps, null)(Home); 