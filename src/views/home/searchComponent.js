import React, { Component } from 'react';
import "./home.css";
import String from '../../assets/locals';
import { Link , Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { endPoint } from "../../assets/apiEndPoint";
import axios from "axios";
import Skeleton from 'react-loading-skeleton';


class SearchComponent extends Component {

    state = {
        navsData: null,
        searchWord: "",
        cities: [],
        countries: [],
        selectedCountry: -1,
        selectedCategory: null,
        subCategoryChildren: [],
        selectedSubCategory: -1,
        selectedCity: -1,
        searchQuery: {},
        redirectToSearchReult: false,
        query: "",
        slider:[],
        img:"",
        skiderLoading:true,
    }
    fetchSlider = () => {
        let uri = `${endPoint}slider`;
        this.setState({skiderLoading:true})
        axios.get(uri)
            .then(res => {
                let images = res.data.data[0].img
                let img = images[Math.floor(Math.random()*images.length)];
                console.log(img)
                this.setState({ skiderLoading:false,slider:res.data.data[0],img:img});
                
            })
            .catch(err => {
                //this.setState({skiderLoading:true})
                console.log("err ", err)
            });
    }

    fetchNavsData() {
        // let categoryUri = `${endPoint}categories?main=true`;
        let countriesUri = `${endPoint}countries`;

        axios.get(countriesUri)
             .then( response => this.setState({countries:response.data.data}))
             .catch(err => console.log("err" , err) ) ;

        // axios.all([axios.get(categoryUri), axios.get(countriesUri)])
        //     .then(allResponses => {
        //         this.setState({
        //             navsData: allResponses[0].data.data,
        //             countries: allResponses[1].data.data
        //         });
        //     })
        //     .catch(err => console.log("err", err));
    }

    fetchCities = (countryId) => {
        let uri = `${endPoint}countries/${countryId}/cities`;
        axios.get(uri)
            .then(res => {
                this.setState({ cities: res.data.data, selectedCity: -1 });
            })
            .catch(err => console.log("err ", err));
    }

    handelUserSearch = (e) => {
        this.setState({ searchWord: e.target.value });
    }

    getUserSearch = () => {

        let query = `?search=${this.state.searchWord.trim()}`;

        // entered empty word       
        if (!this.state.searchWord.trim()) {
            this.setState({ query: "?search=null", redirectToSearchReult: true });
            return;
        }

        if (this.state.selectedCountry !== -1)
            query += `&country=${this.state.countries[this.state.selectedCountry].id}`;

        if (this.state.selectedCategory !== null)
            query += `&category=${this.props.categories[this.state.selectedCategory].id}`;

        if (this.state.selectedSubCategory !== -1)
            query += `&subCategory=${this.state.subCategoryChildren[this.state.selectedSubCategory].id}`;

        this.setState({ query, redirectToSearchReult: true });
    }

    handleSelectCategory = (index) => {
        this.setState({
            selectedCategory: index,
            subCategoryChildren: this.props.categories[index].child,
            selectedSubCategory: -1
        });
    }

    handleSelectCountry = (e) => {
        this.setState({ selectedCountry: e.target.value });
        this.fetchCities(this.state.countries[e.target.value].id);
    }

    componentDidMount() {
        this.fetchNavsData()
        this.fetchSlider()
    }

    render() {

        String.setLanguage(this.props.appLanguage);
        var slider = this.state.slider;
        var img = this.state.img;
       console.log(img)
       
        return (
          
            <div className="home-img-header" 
            style={{backgroundImage: `url(${img})`
            }}>
                {this.state.redirectToSearchReult &&
                    <Redirect to={`/searchResults/keywords${this.state.query}`} />}

                <div className="container text-center p-5 shadow-search" >
                    <p className="home-header-p" > {this.props.appLanguage === "en" ? slider.title : slider.arabicTitle}</p>

                    <ul className="nav justify-content-center home-search "  style={{ padding: "0px" }} >
                        <li className="nav-item serch-text  home-search-list home-no-brdr"  
                            onClick={() => this.setState({
                                selectedCategory: null,
                                selectedCountry: -1,
                                selectedSubCategory: -1,
                                selectedCity: -1
                            })} >

                            {this.state.selectedCategory === null ? String.searchAll : String.search}
                        </li>
                        {this.props.categories && this.props.categories.map((item, index) =>
                            <li key={index} className={"nav-item home-search-list " + (index === this.props.categories.length - 1 ? " home-a" : "")}
                                value={index} onClick={() => this.handleSelectCategory(index)} >

                                <img src={item.img} alt="category" />
                                {this.props.appLanguage === "en" ? item.name : item.arabicName}
                            </li>
                        )}
                    </ul>

                    {this.state.selectedCategory !== null &&
                        <div className="home-select-option py-4 px-2" >

                            <div className="row" >

                                <div className="col-sm-12 col-lg-6 mb-3" >
                                    <select className="form-control" value={this.state.selectedCountry}
                                        onChange={(e) => this.handleSelectCountry(e)} >

                                        <option value={-1} disabled > {String.selectCountry} </option>
                                        {this.state.countries && this.state.countries.map((item, index) =>
                                            <option key={index} value={index} >
                                                {this.props.appLanguage === "en" ? item.countryName : item.arabicName}
                                            </option>
                                        )}

                                    </select>
                                </div>

                                <div className="col-sm-12 col-lg-6 mb-3" >
                                    <select className="form-control" value={this.state.selectedCity}
                                        onChange={(e) => { this.setState({ selectedCity: e.target.value }) }} >

                                        <option value={-1} disabled > {String.selectCity} </option>
                                        {this.state.cities && this.state.cities.map((item, index) =>
                                            <option key={index} value={index} >
                                                {this.props.appLanguage === "en" ? item.cityName : item.arabicName}
                                            </option>
                                        )}

                                    </select>
                                </div>

                                <div className="col-sm-12 col-lg-6 mb-3" >
                                    <select className="form-control" value={this.state.selectedSubCategory}
                                        onChange={(e) => this.setState({ selectedSubCategory: e.target.value })} >

                                        <option value={-1} disabled > {String.adSubCategory} </option>
                                        {this.state.subCategoryChildren && this.state.subCategoryChildren.map((item, index) =>
                                            <option key={index} value={index} >
                                                {this.props.appLanguage === "en" ? item.name : item.arabicName}
                                            </option>
                                        )}

                                    </select>
                                </div>

                            </div>
                        </div>
                    }

                    <div className="nav nav-search-bar">
                        <input className=" form-control" placeholder={String.search} value={this.state.searchWord}
                            onChange={this.handelUserSearch} />
                        <Link to="#" onClick={this.getUserSearch} className=" btn btn-danger nav-link " > {String.search} </Link>
                    </div>
                </div>
            </div>
        
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage , 
        categories : state.headerReducer.categories 
    }
}
export default connect(mapStateToProps, null)(SearchComponent); 
/*
import React, { Component } from 'react';
import "./home.css";
import String from '../../assets/locals';
import { Link , Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { endPoint } from "../../assets/apiEndPoint";
import axios from "axios";
import { Carousel,CarouselItem } from 'react-bootstrap';


class SearchComponent extends Component {

    state = {
        navsData: null,
        searchWord: "",
        cities: [],
        countries: [],
        selectedCountry: -1,
        selectedCategory: null,
        subCategoryChildren: [],
        selectedSubCategory: -1,
        selectedCity: -1,
        searchQuery: {},
        redirectToSearchReult: false,
        query: ""
    }

    fetchNavsData() {
        // let categoryUri = `${endPoint}categories?main=true`;
        let countriesUri = `${endPoint}countries`;

        axios.get(countriesUri)
             .then( response => this.setState({countries:response.data.data}))
             .catch(err => console.log("err" , err) ) ;

        // axios.all([axios.get(categoryUri), axios.get(countriesUri)])
        //     .then(allResponses => {
        //         this.setState({
        //             navsData: allResponses[0].data.data,
        //             countries: allResponses[1].data.data
        //         });
        //     })
        //     .catch(err => console.log("err", err));
    }

    fetchCities = (countryId) => {
        let uri = `${endPoint}countries/${countryId}/cities`;
        axios.get(uri)
            .then(res => {
                this.setState({ cities: res.data.data, selectedCity: -1 });
            })
            .catch(err => console.log("err ", err));
    }

    handelUserSearch = (e) => {
        this.setState({ searchWord: e.target.value });
    }

    getUserSearch = () => {

        let query = `?search=${this.state.searchWord.trim()}`;

        // entered empty word       
        if (!this.state.searchWord.trim()) {
            this.setState({ query: "?search=null", redirectToSearchReult: true });
            return;
        }

        if (this.state.selectedCountry !== -1)
            query += `&country=${this.state.countries[this.state.selectedCountry].id}`;

        if (this.state.selectedCategory !== null)
            query += `&category=${this.props.categories[this.state.selectedCategory].id}`;

        if (this.state.selectedSubCategory !== -1)
            query += `&subCategory=${this.state.subCategoryChildren[this.state.selectedSubCategory].id}`;

        this.setState({ query, redirectToSearchReult: true });
    }

    handleSelectCategory = (index) => {
        this.setState({
            selectedCategory: index,
            subCategoryChildren: this.props.categories[index].child,
            selectedSubCategory: -1
        });
    }

    handleSelectCountry = (e) => {
        this.setState({ selectedCountry: e.target.value });
        this.fetchCities(this.state.countries[e.target.value].id);
    }

    componentDidMount() {
        this.fetchNavsData()
    }

    render() {
 
        String.setLanguage(this.props.appLanguage);


        return (
            <div className="home-img-header" >
                <Carousel>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="https://res.cloudinary.com/ishabrawy/image/upload/v1572089716/fwpddkm5zbqq5svmhaf5.jpg"
                        alt="First slide"
                        />
                        <Carousel.Caption>
                            <div>
                                
                            </div>
                        </Carousel.Caption>
                    </Carousel.Item>
                   
                </Carousel>
                <div className='topSlide'>
                {this.state.redirectToSearchReult &&
                    <Redirect to={`/searchResults/keywords${this.state.query}`} />}

                <div className="container text-center p-5 shadow-search" >
                    <p className="home-header-p" > {String.imgHeaderQoute} </p>

                    <ul className="nav justify-content-center home-search " style={{ padding: "0px" }} >
                        <li className="nav-item serch-text  home-search-list home-no-brdr" style={{ marginTop: "1.1rem" }}
                            onClick={() => this.setState({
                                selectedCategory: null,
                                selectedCountry: -1,
                                selectedSubCategory: -1,
                                selectedCity: -1
                            })} >

                            {this.state.selectedCategory === null ? String.searchAll : String.search}
                        </li>
                        {this.props.categories && this.props.categories.map((item, index) =>
                            <li key={index} className={"nav-item home-search-list " + (index === this.props.categories.length - 1 ? " home-no-brdr" : "")}
                                value={index} onClick={() => this.handleSelectCategory(index)} >

                                <img src={item.img} alt="category" />
                                {this.props.appLanguage === "en" ? item.name : item.arabicName}
                            </li>
                        )}
                    </ul>

                    {this.state.selectedCategory !== null &&
                        <div className="home-select-option py-4 px-2" >

                            <div className="row" >

                                <div className="col-sm-12 col-lg-6 mb-3" >
                                    <select className="form-control" value={this.state.selectedCountry}
                                        onChange={(e) => this.handleSelectCountry(e)} >

                                        <option value={-1} disabled > {String.selectCountry} </option>
                                        {this.state.countries && this.state.countries.map((item, index) =>
                                            <option key={index} value={index} >
                                                {this.props.appLanguage === "en" ? item.countryName : item.arabicName}
                                            </option>
                                        )}

                                    </select>
                                </div>

                                <div className="col-sm-12 col-lg-6 mb-3" >
                                    <select className="form-control" value={this.state.selectedCity}
                                        onChange={(e) => { this.setState({ selectedCity: e.target.value }) }} >

                                        <option value={-1} disabled > {String.selectCity} </option>
                                        {this.state.cities && this.state.cities.map((item, index) =>
                                            <option key={index} value={index} >
                                                {this.props.appLanguage === "en" ? item.cityName : item.arabicName}
                                            </option>
                                        )}

                                    </select>
                                </div>

                                <div className="col-sm-12 col-lg-6 mb-3" >
                                    <select className="form-control" value={this.state.selectedSubCategory}
                                        onChange={(e) => this.setState({ selectedSubCategory: e.target.value })} >

                                        <option value={-1} disabled > {String.adSubCategory} </option>
                                        {this.state.subCategoryChildren && this.state.subCategoryChildren.map((item, index) =>
                                            <option key={index} value={index} >
                                                {this.props.appLanguage === "en" ? item.name : item.arabicName}
                                            </option>
                                        )}

                                    </select>
                                </div>

                            </div>
                        </div>
                    }

                    <div className="nav nav-search-bar">
                        <input className=" form-control" placeholder={String.search} value={this.state.searchWord}
                            onChange={this.handelUserSearch} />
                        <Link to="#" onClick={this.getUserSearch} className=" btn btn-danger nav-link " > {String.search} </Link>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage , 
        categories : state.headerReducer.categories 
    }
}
export default connect(mapStateToProps, null)(SearchComponent); 

 */
