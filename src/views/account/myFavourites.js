import React, { Component } from 'react';
import "./account.css";
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { endPoint } from '../../assets/apiEndPoint';
import axios from 'axios';
import Tabs from './account-tabs';
import String from "../../assets/locals";
import Pagination from "../../views/pagination/pagination";
import CategoryCard from "../../views/category/categoryCard";

class MyFavourites extends Component {

    state = {
        favs: [],
        pageCount: 1,
        currentPage: 1
    }

    componentDidMount() {
        this.fecthMyFavs(this.state.currentPage);
    }

    fecthMyFavs = (page) => {

        let uri = `${endPoint}/favourite/${this.props.currentUser.id}/users?limit=12&page=${page}`;
        axios.get(uri)
            .then(res => {
                if (res.data.data.length === 0 || !res.data.data) this.setState({ favs: null, currentPage: 1, pageCount: 1 });
                else this.setState({ favs: res.data.data, currentPage: res.data.page, pageCount: res.data.pageCount });
            })
            .catch(err => console.log("err", err));
    }

    onItemClick = (page) => {
        this.fecthMyFavs(page);
    }

    onPrevClick = () => {
        this.fecthMyFavs(this.state.currentPage - 1);
    }

    onNextClick = () => {
        this.fecthMyFavs(this.state.currentPage + 1);
    }


    render() {

        String.setLanguage(this.props.appLanguage);

        return (
            <div className="account-profile" >

                <div className="container" >
                    <Tabs tabId={4} />
                </div>

                {!this.props.isLogged && <Redirect to="/" />}

                <div className="row my-5 px-3" >
                    {this.state.favs && this.state.favs.length > 0 && this.state.favs.map((element, index) =>
                        <CategoryCard key={index} ads={element.add} />
                    )}
                </div>

                {this.state.pageCount > 1 &&
                        <Pagination totalPages={this.state.pageCount}
                            currentPage={this.state.currentPage}
                            bullets={3}
                            onItemClick={this.onItemClick}
                            onPrevClick={this.onPrevClick}
                            onNextClick={this.onNextClick} />
                }

                {!this.state.favs &&
                    <div className=" container pt-3 pb-5" >
                        <div className="list-group" >
                            <div className="text-center my-5 p-5" >
                                <strong style={{color:"#679C8A"}}> {String.emptyFavs} </strong>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        currentUser: state.userReducer.user,
        isLogged: state.userReducer.logged
    }
}
export default connect(mapStateToProps, null)(MyFavourites);