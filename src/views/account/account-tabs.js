import React, { Component } from 'react';
import "./account.css";
import { Link } from 'react-router-dom';
import String from "../../assets/locals" ;
import { connect } from 'react-redux';

class AccountTabs extends Component {

    render() {

        String.setLanguage(this.props.appLanguage) ;

        return (
            <div>
                <ul className="nav nav-tabs nav-justified" >
                    <li className="nav-item">
                        <Link className={"nav-link tabs-text-color  " + (this.props.tabId === 1 ? 'active' : '' )} to="/account/myprofile" > {String.myProfile} </Link>
                    </li>
                    <li className="nav-item">
                        <Link className={"nav-link tabs-text-color  " + (this.props.tabId === 2 ? 'active' : '' )} to="/account/myAds" > {String.myAdstab} </Link>
                    </li>
                    {/*}
                    <li className="nav-item">
                        <Link className={"nav-link tabs-text-color " + (this.props.tabId === 3 ? 'active' : '' )} to="/account/myConnection" > {String.myConnections} </Link>
                    </li>
        {*/}
                    <li className="nav-item">
                        <Link className={"nav- tabs-text-color  " + (this.props.tabId === 4 ? 'active' : '' )} to="/account/myfavourites" > {String.myFav} </Link>
                    </li>
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage
    }
}

export default  connect(mapStateToProps,null) (AccountTabs);
 