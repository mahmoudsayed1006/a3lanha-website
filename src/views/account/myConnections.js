import React, { Component } from 'react';
import "./account.css";
import Tabs from './account-tabs';
import { connect } from "react-redux";
import { Link, Redirect } from 'react-router-dom';
// import { ads } from '../../assets/dummyData/account';
import { endPoint, localStorageUserToken } from '../../assets/apiEndPoint';
import axios from 'axios';

class MyConnections extends Component {

    state = {
        myFollowers: []
    }

    fetchMyFollowers = () => {
        let uri = `${endPoint}follow/${this.props.currentUser.id}/followers`;
        axios.get(uri, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem(localStorageUserToken)}`
            }
        })
            .then(response => {
                if (response.data.data.length === 0 || !response.data.data) this.setState({ myFollowers: null });
                else this.setState({ myFollowers: response.data.data })
            })
            .catch(err => console.log("error", err));
    }

    componentDidMount() {
        this.fetchMyFollowers();
    }

    render() {
        return (
            <div className="account-profile" >
                <div className="container">
                    <Tabs tabId={3} />

                    {!this.props.isLogged && <Redirect to="/" />}

                    <div className="pt-3 pb-5" >
                        <div className="list-group" >
                            {this.state.myFollowers && this.state.myFollowers.map((item, index) =>
                                <Link key={index} className="list-group-item list-group-item-action my-2 ads-item-background " to="#" > {item.follower.username}  </Link>
                            )}

                            {/* {!this.state.searchs &&
                                <div className="text-center p-5" >
                                    <strong> no search yet , explore what you need </strong>
                                </div>
                            } */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        currentUser: state.userReducer.user,
        isLogged: state.userReducer.logged
    }
}
export default connect(mapStateToProps, null)(MyConnections);