import React, { Component } from 'react';
import "./account.css";
import Tabs from './account-tabs';
import { Link } from 'react-router-dom';
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { endPoint } from '../../assets/apiEndPoint';
import axios from 'axios';
import String from '../../assets/locals';
import Pagination from "../../views/pagination/pagination";
import CategoryCard from "../../views/category/categoryCard";

class MyAds extends Component {

    state = {
        ads: [],
        pageCount: 1,
        currentPage: 1
    }

    componentDidMount() {
        this.fetchMyAds(this.state.page);
    }

    fetchMyAds = (page) => {
        let uri = `${endPoint}ads?owner=${this.props.currentUser.id}&limit=12&page=${page}`;
        axios.get(uri)
            .then(res => {
                if (res.data.data.length === 0 || !res.data.data) this.setState({ ads: null,currentPage: 1, pageCount: 1 });
                else this.setState({ ads: res.data.data , currentPage: res.data.page, pageCount: res.data.pageCount });
            })
            .catch(err => console.log("err"))
    }

    onItemClick = (page) => {
        this.fetchMyAds(page);
    }

    onPrevClick = () => {
        this.fetchMyAds(this.state.currentPage - 1);
    }

    onNextClick = () => {
        this.fetchMyAds(this.state.currentPage + 1);
    }

    render() {

        String.setLanguage(this.props.appLanguage);

        return (
            <div className="account-profile" >
                <div className="container">
                    <Tabs tabId={2} />
                </div>

                {!this.props.isLogged && <Redirect to="/" />}

                <div className="row my-5 px-3" >
                    {this.state.ads && this.state.ads.length > 0 && this.state.ads.map((element, index) =>
                        <CategoryCard key={index} ads={element} />
                    )}
                </div>

                {this.state.pageCount > 1 &&
                    <Pagination totalPages={this.state.pageCount}
                        currentPage={this.state.currentPage}
                        bullets={3}
                        onItemClick={this.onItemClick}
                        onPrevClick={this.onPrevClick}
                        onNextClick={this.onNextClick} />
                }

                {!this.state.ads &&
                        <div className=" container text-center p-5 my-5 " >
                            <strong> {String.noAds} </strong>
                            <Link to="/placeYourAds" style={{color:"#679C8A"}} > {String.placeYourAdsLink} </Link>
                        </div>
                }

            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        currentUser: state.userReducer.user,
        isLogged: state.userReducer.logged
    }
}
export default connect(mapStateToProps, null)(MyAds);