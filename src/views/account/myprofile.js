import React, { Component } from 'react';
import "./account.css";
import Tabs from './account-tabs';
import String from '../../assets/locals';
import { Redirect, Link } from "react-router-dom";
import { connect } from 'react-redux';
import person from "../../assets/images/person.jpg";


class MyProfile extends Component {


    render() {

        String.setLanguage(this.props.appLanguage);

        return (
            <div className="account-profile" >
                <div className="container">
                    <Tabs tabId={1} />

                    {!this.props.isLogged && <Redirect to="/" />}

                    <div className="d-flex justify-content-between" >
                        <p className="my-4 d-flex" style={{ fontSize: "1.3rem" }} > <strong> {String.hellomsg} {this.props.currentUser && this.props.currentUser.username} </strong> </p>
                        <Link to="/account/updateprofile" > <p className="my-4 d-flex" style={{color:'#679C8A'}}>  {String.accountSettings} </p> </Link>
                    </div>
                    <div className="row pt-3 pb-5" >

                        <div className="col-sm-12 col-md-12 col-lg-2 mb-3" >
                            <div className="profile-img-wrapper d-flex flex-column align-items-center " >
                                <img className="profile-img"
                                    src={this.props.currentUser ? (this.props.currentUser.img ? this.props.currentUser.img : person) : person} alt="user" />
                            </div>
                        </div>

                        <div className="col-sm-12 col-md-12 col-lg-4" >
                            <div className="row" >
                                <div className="col-12 mb-3 d-flex" >
                                    <label > <strong > {String.userName} :</strong> </label>
                                    <span  className="px-1" > {this.props.currentUser && this.props.currentUser.username} </span>
                                </div>

                                <div className="col-12 mb-3 d-flex" >
                                    <label> <strong> {String.email} :</strong> </label>
                                    <span className="px-1"> {this.props.currentUser && this.props.currentUser.email} </span>
                                </div>

                                <div className="col-12 mb-3 d-flex" >
                                    <label> <strong> {String.phone} :</strong> </label>
                                    <span className="px-1"> {this.props.currentUser && this.props.currentUser.phone} </span>
                                </div>

                                <div className="col-12 mb-3 d-flex" >
                                    <label> <strong> {String.country} :</strong> </label>
                                    <span className="px-1"> {this.props.currentUser && this.props.currentUser.country} </span>
                                </div>

                            </div>
                        </div>

                        {/* <div className="col-sm-12 col-md-12 col-lg-6  p-3" >

                            <div className="d-flex flex-row justify-content-around profile-stats-wrapper " >
                                <div className="row flex-column text-center" >
                                    <span> <strong> {String.myAdstab} </strong> </span>
                                    <span style={{ fontSize: "1.2rem" }} > <strong> 12 </strong> </span>
                                    <span  > {String.myAdsViewed} <strong> 2 </strong> {String.times} </span>
                                </div>

                                <div className="row flex-column text-center give-border px-3 " >
                                    <span> <strong> {String.mySearches} </strong> </span>
                                    <span style={{ fontSize: "1.2rem" }} > <strong> {this.props.currentUser.followers.length} </strong> </span>
                                    <span > {String.savedSearches} </span>
                                </div>

                                <div className="row flex-column text-center" >
                                    <span> <strong> {String.myFav} </strong> </span>
                                    <span style={{ fontSize: "1.2rem" }} > <strong> {this.props.currentUser.favourite.length} </strong> </span>
                                    <span > {String.savedAds} </span>
                                </div>
                            </div>

                        </div> */}
                    </div>

                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        currentUser: state.userReducer.user,
        isLogged: state.userReducer.logged 
    }
}


export default connect(mapStateToProps, null)(MyProfile);