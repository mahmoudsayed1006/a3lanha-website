import React, { Component } from 'react';
import "./account.css";
import String from '../../assets/locals';
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { endPoint, localStorageUserToken } from "../../assets/apiEndPoint";
import { bindActionCreators } from 'redux';
import { saveUser } from '../../redux/Actions/userActions';
import axios from 'axios';
import person from "../../assets/images/person.jpg";

class UpdateProfile extends Component {

    state = {
        userName: null,
        email: null,
        country: null,
        phone: null,
        img: person,
        realImg: person,
        countries: null,
        selectedCountry: null,
        errors: {
            userName: null,
            email: null,
            phone: null
        },
        redirectToProfile: false
    }


    componentWillMount() {
        if (this.props.currentUser) {

            let arabicCountries = ["المملكة العربية السعودية", "الامارات", "الكويت", "البحرين", "عمان"];
            let englishCountries = ["Saudi Arabia", "United Arab Emirates", "Kuwait", "Bahrain", "Oman"];

            let countryIndex = 0;
            for (let i = 0; i < arabicCountries.length; i++)
                if (this.props.currentUser.country === arabicCountries[i]) countryIndex = i;

            for (let i = 0; i < englishCountries.length; i++)
                if (this.props.currentUser.country === englishCountries[i]) countryIndex = i;

            this.setState({
                userName: this.props.currentUser.username,
                email: this.props.currentUser.email,
                country: this.props.currentUser.country,
                phone: this.props.currentUser.phone[0],
                img: this.props.currentUser.img ? this.props.currentUser.img : person,
                selectedCountry: countryIndex
            })
        }
    }

    uploadImage = (e) => {
        if (e.target.files[0])
            this.setState({ realImg: e.target.files[0], img: URL.createObjectURL(e.target.files[0]) });
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleValidation = () => {

        let formIsValid = true;
        let errors = {};

        if (!this.state.userName || this.state.userName.trim() === "") {
            formIsValid = false;
            errors.userName = String.nameRequired
        }

        if (!this.state.email || this.state.email.trim() === "") {
            formIsValid = false;
            errors.email = String.emailRequired;
        }
        else if (typeof this.state.email !== "undefined") {
            let lastAtPos = this.state.email.lastIndexOf('@');
            let lastDotPos = this.state.email.lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.email.indexOf('@@') === -1 && lastDotPos > 2 && (this.state.email.length - lastDotPos) > 2)) {
                formIsValid = false;
                errors.email = String.invaildEmail;
            }
        }

        if (!this.state.phone) {
            formIsValid = false;
            errors.phone = String.phoneRequired
        }
        else if (typeof this.state.phone !== "undefined") {
            if (!this.state.phone.match(/^[0-9]+$/)) {
                formIsValid = false;
                errors.phone = String.phoneNumbersOnly;
            }
        }

        this.setState({ errors });
        return formIsValid;
    }

    updateInfoAPI = () => {
        let uri = `${endPoint}user/${this.props.currentUser.id}/updateInfo`;

        let data = new FormData();
        data.append("username", this.state.userName);
        data.append("email", this.state.email);
        data.append("phone", this.state.phone);
        data.append("country", String.countries[this.state.selectedCountry]);
        data.append("img", this.state.realImg);

        axios.put(uri, data, {
            headers: {
                "Authorization": `Bearer  ${localStorage.getItem(localStorageUserToken)}`,
                "Content-Type": "multipart/form-data"
            }
        }).then(res => {
            this.props.saveUser(localStorage.getItem(localStorageUserToken), res.data.user);
            this.setState({ redirectToProfile: true });
        })
            .catch(err => console.log("error happend ", err))
    }

    submitHandle = () => {
        if (this.handleValidation()) {
            this.updateInfoAPI();
        }
    }


    render() {

        String.setLanguage(this.props.appLanguage);

        return (
            <div className="account-profile" >

                {!this.props.isLogged && <Redirect to="/" />}
                {this.state.redirectToProfile && <Redirect to="/account/myprofile" />}

                <div className="container">
                    <div className="row" >

                        <div className="col-sm-12 col-lg-6 mb-3 d-flex flex-column align-items-on-mid-center" >
                            <img src={this.state.img} alt="user" className="update-user-image" />
                            <button className="profile-uplaod-image btn" style={{ backgroundColor: "#679c8ac7", color: "#fff" }} >
                                {String.uploadIamge}
                                <input type="file" accept="image/*" onChange={this.uploadImage} />
                            </button>
                        </div>

                        <div className="col-sm-12 col-lg-6 mb-3 " >
                            <div className="row">

                                <div className="col-12 mb-3 d-flex flex-column " >
                                    <label className="d-flex"  style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end' }}> <strong> {String.userName} </strong> </label>
                                    <input type="text" className="form-control" onChange={this.handleChange} name="userName" value={this.state.userName}  />
                                    <p className="d-flex" >
                                        <span style={{ color: "red" }}>{this.state.errors["userName"]}</span>
                                    </p>
                                </div>

                                <div className="col-12 mb-3 d-flex flex-column" >
                                    <label className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end' }}> <strong> {String.email} </strong> </label>
                                    <input type="text" className="form-control" onChange={this.handleChange} name="email" value={this.state.email} />
                                    <p className="d-flex" >
                                        <span style={{ color: "red" }}>{this.state.errors["email"]}</span>
                                    </p>
                                </div>

                                <div className="col-12 mb-3 d-flex flex-column" >
                                    <label className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end' }}> <strong> {String.phone} </strong> </label>
                                    <input type="text" className="form-control" onChange={this.handleChange} name="phone" value={this.state.phone} />
                                    <p className="d-flex" >
                                        <span style={{ color: "red" }}>{this.state.errors["phone"]}</span>
                                    </p>
                                </div>

                                <div className="col-12 mb-3 d-flex flex-column" >

                                    <label className="d-flex" style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end' }}> <strong> {String.country} </strong> </label>
                                    <select className="form-control" onChange={(e) => this.setState({ selectedCountry: e.target.value })}
                                        value={this.state.selectedCountry} >
                                        {String.countries.map((item, index) => <option value={index} key={index} > {item} </option>)}
                                    </select>

                                </div>

                            </div>
                        </div>
                        <div className="col-12 d-flex justify-content-right mt-3 mb-5 " style={{display:'flex',justifyContent:this.props.appLanguage === "en" ? 'flex-start':'flex-end' }}>
                            <button onClick={this.submitHandle} className="btn px-5" style={{ backgroundColor: "#679c8ac7", color: "#fff" ,width:'48.5%'}}> {String.saveChanges} </button>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage,
        currentUser: state.userReducer.user,
        isLogged: state.userReducer.logged
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {
        saveUser
    },
    dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile);
