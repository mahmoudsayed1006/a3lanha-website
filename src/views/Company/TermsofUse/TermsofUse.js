import React, { Component } from 'react';
import './TermsofUse.css';
import logo from '../../../assets/images/Logogreen.png';
import { endPoint } from "../../../assets/apiEndPoint";
import axios from 'axios';
import { Link } from 'react-router-dom'
import String from '../../../assets/locals';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'

class TermsofUse extends Component {

    state = {
        TermsofUseData: []
    }

    fetchData() {
        let uri = `${endPoint}about`
        let token = ''
        axios.get(uri, {
            headers: {
                Authorization: token,
            }
        }).then(response => {
            this.setState({ TermsofUseData: response.data });
        })
    }

    componentDidMount() {
        this.fetchData()
    }

    render() {
        String.setLanguage(this.props.appLanguage);
        return (
            <div className="TermsofUse">
                <div className="TermsofUse-header text-center py-3">
                    <div><Link to="/"><img width="100" src={logo} alt="logo" /></Link></div>
                </div>
                <div className="TermsofUse-content mb-5">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-12 text-center mt-5 mb-3">
                                <h2>{String.termsOfUs}</h2>
                            </div>
                            {this.state.TermsofUseData.map((element, index) => {
                                return (
                                    <div key={index} className="col-lg-6 col-sm-12 mx-auto">
                                        {element.conditions}
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        appLanguage: state.headerReducer.appLanguage
    }
}
const mapDispatchToProps = dispatch => bindActionCreators(
    {
        
    },
    dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(TermsofUse);